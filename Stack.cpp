
/*
Name: Stack.cpp
Copyright: Amit Malyala, 2016-Current. All rights reserved.
Author: Amit Malyala
Date: 18-11-16 01:50
Version: 
0.01 Initial version
0.02 Added stacks for OriginalSourceStack, FileStack, IntermediateHeaderstack and HeaderStack.

Reference: 
Foundations of computer science - Ullman and Aho 
		 
Description:
Stack implementation using vector and std::stack.
To do:
Create another version which uses pointers. Modify these functions for construction of AST.
Create a operand and operator stack using std::vector
Change the stack to create a stack with linked list or std::stack than using arrays. Limit the size of the stack 
to the size of macro expression.

Notes:

 */

#include "std_types.h"
#include <iostream>
#include "stack.h"
#include <vector>
#include "Lexer.h"
#include "Error.h"
#include "parser.h"

/* Header files in original source file . These headers are at root level.*/
std::vector <std::string> OriginalSourceStack;

/* A stack of std::strings for storing filenames during processing*/
std::vector <std::string> FileStack;

/* A Stack of headers */
std::vector <HeaderData> HeaderStack;

/* A stack of nested headers list for headers processing */
std::vector <HeaderData> IntermediateHeaderStack;

/*
Component Function: static BOOL isIntermediateHeaderStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isIntermediateHeaderStackFull(void);

/*
Component Function: static void ClearHeaderStack(std::vector <HeaderData> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearHeaderStack(std::vector <HeaderData> & pS);

/*
Component Function: static BOOL isHeaderStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isHeaderStackFull(void);

/*
Component Function: static void ClearHeaderStack(std::vector <HeaderData> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearHeaderStack(std::vector <HeaderData> & pS);

/*
Component Function: static BOOL isFileStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isFileStackFull(void);

/*
Component Function: BOOL Stack::isOriginalSourceStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL Stack::isOriginalSourceStackEmpty(void);

/*
Component Function: static BOOL isOriginalSourceStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isOriginalSourceStackFull(void);

/* Comment to integrate with the project, UnComment to test */
#if(0)

SINT32 main(void)
{
    /* General Init functions for stacks */
    InitStack();
    if (CheckforErrors())
    {
        ErrorTracer::PrintErrors();
    }
    return 0;
}
#endif

/*
Component Function: void InitStack(void)
Arguments:  None
Returns: None
Description:  Initializes Operator and RPN stack
Version: 0.1 Initial version
 */
void InitStack(void)
{
    Stack::ClearFileStack();
    ClearHeaderStack(HeaderStack);
    ClearHeaderStack(IntermediateHeaderStack);

}

/*
Component Function: void Stack::ClearFileStack(void)
Arguments:  None
Returns: true or not
Description:  Clears file stack
Version : 0.1
 */
void Stack::ClearFileStack(void)
{
    FileStack.clear();
}

/*
Component Function: BOOL Stack::isFileStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL Stack::isFileStackEmpty(void)
{
    return FileStack.empty();
}

/*
Component Function: BOOL Stack::PopFileStack(std::string& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL Stack::PopFileStack(std::string& px)
{
    BOOL ReturnValue = false;
    if (isFileStackEmpty())
    {

        ReturnValue = false;
    } else
    {
        px = FileStack.back();
        FileStack.pop_back();
        ReturnValue = true;
    }
    /*
      #if defined (STACK_DEBUGGING_ENABLED)
        //std::cout << "\nPos is at " << HeaderStack.Pos << std::endl;
    #endif
     */
    return ReturnValue;
}

/*
Component Function: BOOL Stack::PushHeaderStack(const std::string& px)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL Stack::PushFileStack(const std::string& px)
{
    BOOL ReturnValue = false;
    if (isFileStackFull())
    {
        /* add correct line number in this function */
        ErrorTracer::ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
        ReturnValue = false;
    } else
    {
        FileStack.push_back(px);
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: static BOOL isFileStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isFileStackFull(void)
{
    UINT32 size = FileStack.size();
    BOOL ReturnValue = false;
    if (size == MAX)
    {
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
 Component Function: void void Stack::ReverseFileStack(void)
 Arguments:  vector of std::string to be reversed.
 returns: None
 Description:
 Reverses a vector
 Version : 0.1
 Notes:
 */
void Stack::ReverseFileStack(void)
{
    std::string temp;
    SINT32 i = 0;
    SINT32 j = 0;
    // Physically modify and reverse vector object
    for (j = 0, i = (FileStack.size() - 1); j < i; --i, ++j)
    {
        temp = FileStack[i];
        FileStack[i] = FileStack[j];
        FileStack[j] = temp;
    }
}

/*
Component Function: void Stack::PrintFileStack(void)
Arguments:  None
Returns: None
Description:  Displays Op stack.
Version : 0.1
Notes:
 */
void Stack::PrintFileStack(void)
{
    DisplayFileStack(FileStack);
}

/*
Component Function: void Stack::DisplayFileStack(const std::vector <Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  
Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void Stack::DisplayFileStack(const std::vector <std::string> &pS)
{
    std::cout << "File stack is:" << std::endl;
    SINT32 index = 0;
    if (!(isFileStackEmpty()))
    {
        for (index = pS.size() - 1; index >= 0; index--)
        {
            std::cout << "Header name " << pS[index] << std::endl;
        }
    } else
    {
        std::cout << "FileStack empty" << std::endl;
    }
}

/*
Component Function: void Stack::ClearOriginalSourceStack(void)
Arguments:  None
Returns: true or not
Description:  Clears Original source file stack
Version : 0.1
 */
void Stack::ClearOriginalSourceStack(void)
{
    OriginalSourceStack.clear();
}

/*
Component Function: BOOL Stack::isOriginalSourceStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL Stack::isOriginalSourceStackEmpty(void)
{
    return OriginalSourceStack.empty();
}

/*
Component Function: BOOL Stack::PopOriginalSourceStack(std::string& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL Stack::PopOriginalSourceStack(std::string& px)
{
    BOOL ReturnValue = false;
    if (isOriginalSourceStackEmpty())
    {

        ReturnValue = false;
    } else
    {
        px = OriginalSourceStack.back();
        OriginalSourceStack.pop_back();
        ReturnValue = true;
    }
    /*
      #if defined (STACK_DEBUGGING_ENABLED)
        //std::cout << "\nPos is at " << HeaderStack.Pos << std::endl;
    #endif
     */
    return ReturnValue;
}

/*
Component Function: BOOL Stack::PushOriginalSourceStack(const std::string& px)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL Stack::PushOriginalSourceStack(const std::string& px)
{
    BOOL ReturnValue = false;
    if (isOriginalSourceStackFull())
    {
        /* add correct line number in this function */
        ErrorTracer::ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
        ReturnValue = false;
    } else
    {
        OriginalSourceStack.push_back(px);
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: static BOOL isOriginalSourceStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isOriginalSourceStackFull(void)
{
    UINT32 size = OriginalSourceStack.size();
    BOOL ReturnValue = false;
    if (size == MAX)
    {
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
 Component Function: void Stack::ReverseOriginalSourceStack(void)
 Arguments:  vector of std::string to be reversed.
 returns: None
 Description:
 Reverses a vector
 Version : 0.1
 Notes:
 */
void Stack::ReverseOriginalSourceStack(void)
{
    std::string temp;
    SINT32 i = 0;
    SINT32 j = 0;
    // Physically modify and reverse vector object
    for (j = 0, i = (OriginalSourceStack.size() - 1); j < i; i--, j++)
    {
        temp = OriginalSourceStack[i];
        OriginalSourceStack[i] = OriginalSourceStack[j];
        OriginalSourceStack[j] = temp;
    }
}

/*
Component Function: void Stack::PrintOriginalSourceStack(void)
Arguments:  None
Returns: None
Description:  Displays Op stack.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void Stack::PrintOriginalSourceStack(void)
{
    DisplayOriginalSourceStack(OriginalSourceStack);
}

/*
Component Function: void Stack::DisplayOriginalSourceStack(const std::vector <Token_tag> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  
Displays Op stack.
Version : 0.1
Notes:
.
 */
void Stack::DisplayOriginalSourceStack(const std::vector <std::string> &pS)
{
    std::cout << "\nHeaders in Original source stack are:" << std::endl;
    SINT32 index = 0;
    if (!(isOriginalSourceStackEmpty()))
    {
        for (index = pS.size() - 1; index >= 0; index--)
        {
            std::cout << "Header name " << pS[index] << std::endl;
        }
    } else
    {
        std::cout << "\nOriginalSourceStack empty";

    }
}

/*
Component Function: BOOL Stack::isHeaderStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL Stack::isHeaderStackEmpty(void)
{
    return HeaderStack.empty();
}

/*
Component Function: BOOL Stack::PopHeaderStack(HeaderData& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL Stack::PopHeaderStack(HeaderData& px)
{
    BOOL ReturnValue = false;
    if (isHeaderStackEmpty())
    {

        ReturnValue = false;
    } else
    {
        px = HeaderStack.back();
        HeaderStack.pop_back();
        ReturnValue = true;
    }
    /*
      #if defined (STACK_DEBUGGING_ENABLED)
        //std::cout << "\nPos is at " << HeaderStack.Pos << std::endl;
    #endif
     */
    return ReturnValue;
}

/*
Component Function: BOOL Stack::PushHeaderStack(const HeaderData& px)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL Stack::PushHeaderStack(const HeaderData& px)
{
    BOOL ReturnValue = false;
    if (isHeaderStackFull())
    {
        /* add correct line number in this function */
        ErrorTracer::ErrorTracer(STACK_ACCESS_ERROR, 1, 1);
        ReturnValue = false;
    } else
    {
        HeaderStack.push_back(px);
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: static BOOL isHeaderStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isHeaderStackFull(void)
{
    UINT32 size = HeaderStack.size();
    BOOL ReturnValue = false;
    if (size == MAX)
    {
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: static void ClearHeaderStack(std::vector <HeaderData> & pS)
Arguments:  Reference to stack
Returns: None
Description:  Clears a stack. Use this at init
Version : 0.1
 */
static void ClearHeaderStack(std::vector <HeaderData> & pS)
{
    pS.clear();
}

/*
Component Function: void Stack::PrintHeaderStack(void)
Arguments:  None
Returns: None
Description:  Displays Header stack.
Version : 0.1
Notes:
This function would print header stack.
 */
void Stack::PrintHeaderStack(void)
{
    //std::cout << "in PrintPostfixStack" << std::endl;
    DisplayHeaderStack(HeaderStack);
}

/*
Component Function: void Stack::DisplayHeaderStack(const std::vector <HeaderData> &pS)
Arguments:  Reference to stack.
Returns: None
Description:  Displays Header stack
Version : 0.1
Notes:
 */
void Stack::DisplayHeaderStack(const std::vector <HeaderData> &pS)
{
    std::cout << "Header source tree is:" << std::endl;
    SINT32 index = 0;
    UINT32 Count=0;
    if (!(isHeaderStackEmpty()))
    {
        for (index = pS.size() - 1; index >= 0; index--)
        {
        	//#if(0)
        	
        	for(Count=0;Count<(pS[index].IndentationLevel)*4;Count++)
        	{
        		std::cout<< " ";
			}
			//#endif
			
            std::cout << pS[index].HeaderName << /* " - (" << pS[index].IndentationLevel << ")"  << */ std::endl;
            // Use this code while filling Postfix and operator stack.
        }
    } else
    {
        std::cout << "Header stack empty" << std::endl;

    }
}

/*
 Component Function: void Stack::ReverseHeaderStack(void)
 Arguments:  vector of HeaderData to be reversed.
 returns: None
 Description:
 Reverses a vector
 Version : 0.1
 Notes:
 */
void Stack::ReverseHeaderStack(void)
{
    HeaderData temp;
    SINT32 i = 0;
    SINT32 j = 0;
    // Physically modify and reverse vector object
    for (j = 0, i = (HeaderStack.size() - 1); j < i; --i, ++j)
    {
        temp = HeaderStack[i];
        HeaderStack[i] = HeaderStack[j];
        HeaderStack[j] = temp;
    }
}


/*
Component Function: BOOL Stack::isHeaderStackEmpty(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
BOOL Stack::isIntermediateHeaderStackEmpty(void)
{
    return IntermediateHeaderStack.empty();
}

/*
Component Function: BOOL Stack::PopHeaderIntermediateStack(HeaderData& px)
Arguments:  Reference to data being popped
Returns: true or not
Description:  returns true if pop is successful
Version : 0.1
 */
BOOL Stack::PopHeaderIntermediateStack(HeaderData& px)
{
    BOOL ReturnValue = false;
    if (isIntermediateHeaderStackEmpty())
    {
        ReturnValue = false;
    } else
    {
        px = IntermediateHeaderStack.back();
        IntermediateHeaderStack.pop_back();
        ReturnValue = true;
    }
    /*
      #if defined (STACK_DEBUGGING_ENABLED)
        //std::cout << "\nPos is at " << HeaderStack.Pos << std::endl;
    #endif
     */
    return ReturnValue;
}

/*
Component Function: BOOL Stack::PushHeaderStack(const HeaderData& px)
Arguments:  Reference to data being pushed
Returns: true or not
Description:  returns true if push is successful
Version : 0.1
 */
BOOL Stack::PushHeaderIntermediateStack(const HeaderData& px)
{
    BOOL ReturnValue = false;
    if (isIntermediateHeaderStackFull())
    {
        /* add correct line number in this function */
        ErrorTracer::ErrorTracer(STACK_ACCESS_ERROR, 0, 0);
        ReturnValue = false;
    } else
    {
        IntermediateHeaderStack.push_back(px);
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: static BOOL isIntermediateHeaderStackFull(void)
Arguments:  None
Returns: true or not
Description:  returns true if empty
Version : 0.1
 */
static BOOL isIntermediateHeaderStackFull(void)
{
    UINT32 size = IntermediateHeaderStack.size();
    BOOL ReturnValue = false;
    if (size == MAX)
    {
        ReturnValue = true;
    }
    return ReturnValue;
}

/*
Component Function: void Stack::PrintIntermediateStack(void)
Arguments:  None
Returns: None
Description:  Displays Intermediate header stack.
Version : 0.1
Notes:
This function would print header stack.
 */
void Stack::PrintIntermediateStack(void)
{
    //std::cout << "in PrintPostfixStack" << std::endl;
    Stack::DisplayIntermediateStack(IntermediateHeaderStack);
}

/*
Component Function: void Stack::DisplayIntermediateStack(const std::vector <HeaderData> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  Displays Header stack
Version : 0.1
Notes:
 */
void Stack::DisplayIntermediateStack(const std::vector <HeaderData> &pS)
{
    std::cout << "Intermediate Header stack is:" << std::endl;
    SINT32 index = 0;
    if (!(isIntermediateHeaderStackEmpty()))
    {
        for (index = pS.size() - 1; index >= 0; index--)
        {
            std::cout << "Header name " << pS[index].HeaderName << std::endl;
            // Use this code while filling Postfix and operator stack.
        }
    } else
    {
        std::cout << "Intermediate Header stack empty" << std::endl;

    }
}



/*
 Component Function: void Stack::ReverseIntermediateHeaderStack(void)
 Arguments:  vector of HeaderData to be reversed.
 returns: None
 Description:
 Reverses a vector
 Version : 0.1
 Notes:
 */
void Stack::ReverseIntermediateHeaderStack(void)
{
    HeaderData temp;
    SINT32 i = 0;
    SINT32 j = 0;
    // Physically modify and reverse vector object
    for (j = 0, i = (IntermediateHeaderStack.size() - 1); j < i; --i, ++j)
    {
        temp = IntermediateHeaderStack[i];
        IntermediateHeaderStack[i] = IntermediateHeaderStack[j];
        IntermediateHeaderStack[j] = temp;
    }
}
