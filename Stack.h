/*
Name:Stack.h
Copyright:
Author:Amit Malyala
Date: 07-01-17 19:59
Description:
 */

#ifndef STACK_H
#define STACK_H

#include "std_types.h"
#include "Lexer.h"
#include "PreProcessor.h"
#include <stack>

//#define STACK_DEBUGGING_ENABLED 1

#if __cplusplus >= 201103L
/* 10000 operands and operators are enough for macro processor with less than 100 expressions
   If there are stack access errors, increase number of operators and operands. */
/* For compiler AST, use a vector for stacks 
#define MAX 10000
*/

/* We will read upto 256 headers and 256 nested headers for each nested header. */
/* This is a stack overflow limit for number of headers and nested headers read */
#define MAX 16777216 

/* Stack functions */
/*
Component Function: void InitStack(void)
Arguments:  None
Returns: None
Description:  Initializes Operator and RPN stack
Version: 0.1 Initial version
 */
void InitStack(void);
/* Definitions for composite data in stack which contains operands and operators for  RPN stack*/


/* Stack module */
namespace Stack 
{
    /*
    Component Function: void Stack::ClearFileStack(void)
    Arguments:  None
    Returns: true or not
    Description:  Clears file stack
    Version : 0.1
    */
    void ClearFileStack(void);
    /*
    Component Function: BOOL isFileStackEmpty(void)
    Arguments:  None
    Returns: true or not
    Description:  returns true if empty
    Version : 0.1
     */
    BOOL isFileStackEmpty(void);
    /*
    Component Function: BOOL PopFileStack(std::string& px)
    Arguments:  Reference to data being popped
    Returns: true or not
    Description:  returns true if pop is successful
    Version : 0.1
     */
    BOOL PopFileStack(std::string& px);
    /*
    Component Function: BOOL PushHeaderStack(const std::string& px)
    Arguments:  Reference to data being pushed
    Returns: true or not
    Description:  returns true if push is successful
    Version : 0.1
     */
    BOOL PushFileStack(const std::string& px);
    
    /*
    Component Function: void Stack::ReverseFileStack(void)
    Arguments:  None
    returns: None
    Description:
    Reverses a vector
    Version : 0.1
    Notes:
    */
    void ReverseFileStack(void);
    
    
    /*
    Component Function: vvoid PrintFileStack(void)
    Arguments:  None
    Returns: None
    Description:  Displays Op stack.
    Version : 0.1
    Notes:
    This function would be used while construction of AST.
    */
    void PrintFileStack(void);
    
     /*
     Component Function: void DisplayFileStack(const std::vector <Token_tag> &pS)
     Arguments:  Reference to stack.
     Returns: true or not
     Description:  
     Displays Op stack.
     Version : 0.1
     Notes:
     This function would be used while construction of AST.
     */
     void DisplayFileStack(const std::vector <std::string> &pS);
    
	/*
    Component Function: void ClearOriginalSourceStack(void)
    Arguments:  None
    Returns: true or not
    Description:  Clears Original source file stack
    Version : 0.1
    */
    void ClearOriginalSourceStack(void);
    /*
    Component Function: BOOL isOriginalSourceStackEmpty(void)
    Arguments:  None
    Returns: true or not
    Description:  returns true if empty
    Version : 0.1
    */
    BOOL isOriginalSourceStackEmpty(void);
    
    /*
    Component Function: BOOL PopOriginalSourceStack(std::string& px)
    Arguments:  Reference to data being popped
    Returns: true or not
    Description:  returns true if pop is successful
    Version : 0.1
    */
    BOOL PopOriginalSourceStack(std::string& px);
    
    /*
    Component Function: BOOL PushOriginalSourceStack(const std::string& px)
    Arguments:  Reference to data being pushed
    Returns: true or not
    Description:  returns true if push is successful
    Version : 0.1
    */
    BOOL PushOriginalSourceStack(const std::string& px);
    
    /*
    Component Function: void PrintOriginalSourceStack(void)
    Arguments:  None
    Returns: None
    Description:  Displays Op stack.
    Version : 0.1
    Notes:
    This function would be used while construction of AST.
    */
    void PrintOriginalSourceStack(void);

    /*
     Component Function: void DisplayOriginalSourceStack(const std::vector <Token_tag> &pS)
     Arguments:  Reference to stack.
     Returns: true or not
     Description:  
     Displays Op stack.
     Version : 0.1
     Notes:
     .
     */
    void DisplayOriginalSourceStack(const std::vector <std::string> &pS);
    
    /*
    Component Function: void ReverseOriginalSourceStack(void)
    Arguments:  vector of std::string to be reversed.
    returns: None
    Description:
    Reverses a vector
    Version : 0.1
    Notes:
    */
    void ReverseOriginalSourceStack(void);

    /*
    Component Function: BOOL isHeaderStackEmpty(void)
    Arguments:  None
    Returns: true or not
    Description:  returns true if empty
    Version : 0.1
     */
    BOOL isHeaderStackEmpty(void);
    
    /*
    Component Function: BOOL PopHeaderStack(HeaderData& px)
    Arguments:  Reference to data being popped
    Returns: true or not
    Description:  returns true if pop is successful
    Version : 0.1
     */
    BOOL PopHeaderStack(HeaderData& px);
    /*
    Component Function: BOOL PushHeaderStack(const HeaderData& px)
    Arguments:  Reference to data being pushed
    Returns: true or not
    Description:  returns true if push is successful
    Version : 0.1
     */
    BOOL PushHeaderStack(const HeaderData& px);
    
    /*
    Component Function: void PrintHeaderStack(void)
    Arguments:  None
    Returns: None
    Description:  Displays Header stack.
    Version : 0.1
    Notes:
    This function would print header stack.
    */
    void PrintHeaderStack(void);
    /*
    Component Function: void DisplayHeaderStack(const std::vector <HeaderData> &pS)
    Arguments:  Reference to stack.
    Returns: true or not
    Description:  Displays Header stack
    Version : 0.1
    Notes:
    */
   void DisplayHeaderStack(const std::vector <HeaderData> &pS);
   
    /*
    Component Function: void ReverseHeaderStack(void)
    Arguments:  vector of HeaderData to be reversed.
    returns: None
    Description:
    Reverses a vector
    Version : 0.1
    Notes:
    */
    void ReverseHeaderStack(void);
    
    /*
    Component Function: BOOL isHeaderStackEmpty(void)
    Arguments:  None
    Returns: true or not
    Description:  returns true if empty
    Version : 0.1
     */
    BOOL isIntermediateHeaderStackEmpty(void);

    /*
    Component Function: BOOL PopHeaderIntermediateStack(HeaderData& px)
    Arguments:  Reference to data being popped
    Returns: true or not
    Description:  returns true if pop is successful
    Version : 0.1
     */
    BOOL PopHeaderIntermediateStack(HeaderData& px);
    /*
    Component Function: BOOL PushHeaderStack(const HeaderData& px)
    Arguments:  Reference to data being pushed
    Returns: true or not
    Description:  returns true if push is successful
    Version : 0.1
     */
    BOOL PushHeaderIntermediateStack(const HeaderData& px);
    /*
    Component Function: void PrintIntermediateStack(void)
    Arguments:  None
    Returns: None
    Description:  Displays Header stack.
    Version : 0.1
    Notes:
    This function would print header stack.
    */
    void PrintIntermediateStack(void);
    /*
    Component Function: void DisplayIntermediateStack(const std::vector <HeaderData> &pS)
    Arguments:  Reference to stack.
    Returns: true or not
    Description:  Displays Header stack
    Version : 0.1
    Notes:
    */
   void DisplayIntermediateStack(const std::vector <HeaderData> &pS);
    
    /*
    Component Function: void ReverseIntermediateHeaderStack(void)
    Arguments:  vector of HeaderData to be reversed.
    returns: None
    Description:
    Reverses a vector
    Version : 0.1
    Notes:
    */
   void ReverseIntermediateHeaderStack(void);
    

} /* namespace Stack */
#endif /* #if __cplusplus >= 201103L */



#endif
