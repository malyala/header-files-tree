/*
Name :  CommentsProcessing.cpp
Author: Amit Malyala ,Copyright Amit Malyala 2016-Current.
Date : 14-11-16 02:08
Description:
This module reads comments and deletes them in a C++ source or header file.
To do:

Known issues:
None
Unknown issues:
None
*/

#include "CommentsProcessing.h"
#include "io.h"
/* Program String */
extern std::vector <UINT8> s;
/* Vector to store line numbers of Double slash comment lines */
std::vector <UINT32> CPPComments;
/* Vector to store line numbers of C-style comments */
std::vector <UINT32> CStyleComments;
/* Misc data of each file processed */
struct FileData CurrentFile;

/* Data of each header */
std::vector <FileData> ListofHeaderFiles;

/* Words of a header */
extern std::vector <std::string> WordList;

// Comment this block to integrate with preprocessor project.
#if(0)
SINT32 main(void)
{
   std::string Filename ="Filename.h";
   HeaderData aHeader;
   HeadersProcessing::ClearHeaderData(aHeader);
   // Read also from a file by modifying the lines below.
   //std::cout << "You entered: " << std::endl;
   //io::DisplayOutput(s);
   CommentsProcessing::InitCommentsProcessing(Filename);
   
   return 0;
}
#endif

/*
Component Function:void CommentsProcessing::InitCommentsProcessing(std::string & Filename)
Arguments: Reference to Filename
returns: None
Description:
Initialize Preprocessor and start to remove comments in source or header files
Bug and Version history :
0.1 Initial version
*/
void CommentsProcessing::InitCommentsProcessing(std::string & Filename)
{
   UINT32 PreProcessorErrors=false;
   UINT32 NumberofCommentLines=0;
   UINT32 NumberofNonCommentLines=0;
   UINT32 NumberofTotalLines=0;
   CPPComments.clear();
   CStyleComments.clear();
   // Read also from a file by modifying the lines below.
   io::GetInput(Filename);
   
   if (ErrorTracer::CheckforErrors() == NO_ERRORS)
   {
   	
    //std::cout << "\nPreprocessor is removing comments: " << std::endl;
   PreProcessorErrors=RemoveCppComments(s,NumberofCommentLines,NumberofNonCommentLines,NumberofTotalLines);
   //std::cout << "\nAfter Comments processing: " << std::endl;
   if (PreProcessorErrors)
   {
   	  std::cout<< std::endl << "Errors Found. Please see above!";
   }
   else
   {
   	  //io::DisplayOutput(s);
   }
   
   /* Debugging data */
   CurrentFile.Filename=Filename;
   /*
   std::cout << "File name: " << CurrentFile.Filename << std::endl;
   std::cout << "Number of C-style comment lines " << CurrentFile.NumberofCStyleComments<< std::endl;
   std::cout << "Number of C++ comment lines " << CurrentFile.NumberofCPPComments<< std::endl;  
   std::cout << "Number of comment lines " << CurrentFile.NumberofCommentLines<< std::endl;
   std::cout << "Number of non comment lines " << CurrentFile.NumberofNonCommentLines<< std::endl;
   std::cout << "Number of total lines " << CurrentFile.NumberofLines<< std::endl;
   system("pause");
   */
   
   /* Add each header name into a vector */
   AddHeaderIntoList(CurrentFile);
   
   
  }
}


/*
Component Function: UINT32 CommentsProcessing::RemoveCppComments(std::vector <UINT8> & s,UINT32&  NumberofCommentLines, UINT32& NumberofNonCommentLines,UINT32& NumberofTotalLines)
Arguments: Reference to vector s which contains the C++ program text
returns: None
Description:
Read through a comment, parse text under comments and erase.
This is a function to parse comments and nested comments.
Bug and Version history :
         0.1 Initial version
         0.2 Added nested comment support
		 0.3 Fixed a feature in which end comment is now detected without a begin comment.
To do:
Add nested loops support 
*/
UINT32 CommentsProcessing::RemoveCppComments(std::vector <UINT8> & s,UINT32&  NumberofCommentLines, UINT32& NumberofNonCommentLines,UINT32& NumberofTotalLines)
{
    // Implement missing comment detection for /* or */ tokens
    UINT32 i = ZERO;
    static UINT32 BeginComments=ZERO;
    static UINT32 EndComments=ZERO;
    UINT32 LineNumber=ONE;
    //UINT32 SlashErrors=NO_ERRORS;
    UINT32 CurrentPass=FIRST_PASS;
    UINT32 NumOfPasses=ONE;
    UINT32 ErrorsFound=false;
    UINT32 NumberofCStyleCommentLines=ZERO;
    UINT32 NumberofDoubleSlashCommentLines=ZERO;
    static UINT32 LineNumberofEndComment = ZERO;
    static UINT32 LineNumberofDoubleSlashComment = ZERO;

    // The number of preprocessor passes can be increased to do enhanced preprocessing with additional features.
    for (NumOfPasses=FIRST_PASS;NumOfPasses<=NUMBER_OF_PREPROCESSOR_PASSES && ErrorsFound==false;NumOfPasses++)
    {

	 if (NumOfPasses==FIRST_PASS)
	 {
	 	CurrentPass=FIRST_PASS;
	 	//std::cout << "\nNumber of preprocessor passes" << NumOfPasses;
	 }
	 else if (NumOfPasses==SECOND_PASS)
	 {
	 	CurrentPass=SECOND_PASS;
	 	//std::cout << "\nNumber of preprocessor passes" << NumOfPasses;
	 }
	 
	 
     
    //#endif
    //#if(0)
    //#if(0)
    i=0;  // Reset loop counter.
    // Restart the scan for /* */ comments
	LineNumber=1;
      // Other pass through the program to detect  and remove /**/ comments
    while( i <= s.size())
    {
      // Code to detect /*  comment
      if(s[i]=='/' && s[i+1]== '*')
      {

        //#if(0)
        // Diagnostic messages.
        //std::cout << "Begin comment /* detected. " << std::endl;
        BeginComments++;
       // #endif
        i=CommentsProcessing::BeginCommentRead(s,i,BeginComments,EndComments,LineNumber,CurrentPass, NumberofCStyleCommentLines,NumberofDoubleSlashCommentLines,LineNumberofEndComment);

        // Now detect comments after comments such as /*/**/*//**/
       if (s[i] =='/' && s[i+1] =='*')
       {
       	 BeginComments++;
         continue;
       }

      }
      else if (s[i] == '/' && s[i+1] == '/')
      {
      	LineNumberofDoubleSlashComment=LineNumber;
	  }
      // Detect if */ Comment starts first without a begin comment /*
      else if (s[i]== '*' && s[i+1] == '/')
      {
      	EndComments++;
      	// Prevent an error due to * / as in this line: // Check if the op beneath a unary minus is a / or */
      	if (LineNumberofDoubleSlashComment!=LineNumber)
      	{
      		ErrorTracer::ErrorTracer(MISSING_COMMENT, LineNumber, 1);
	    }
	  }

	  	if (s[i] == '\n')
    	{
    		LineNumber++;
		}

      i++;
    }
    
     // start the scan for // comments
     i=0;  // Reset loop counter.
    
	LineNumber=1;
    // Second pass through the program to detect and  remove '//' comments
    while(i < s.size())
    {

      // Remove // comments
          if(s[i] == '/' && s[i+1]== '/')
          {

			  //#if(0)
              // Diagnostic messages.
              // std::cout << "Begin comment // detected. " << std::endl;
             // #endif

              while(s[i] != '\n' && i<s.size())
              {

                 // #if(0)
                  // Diagnostic message
                  //std::cout<< " Erase byte " << std::endl;
                 // #endif
                 if (CurrentPass==SECOND_PASS)
                 {
                 	s[i] = ' '; // Erase all bytes until end of line.

				 }

                 i++;
              }
              // Code edit here
              
               if (CurrentPass==SECOND_PASS)
               {
               	  NumberofDoubleSlashCommentLines++;
               	  CPPComments.push_back(LineNumber);
			   }
			  
			   // Code edit end
              
          }

		  // Skip C-style comments in detecting // comments
		  else if ( (s[i]=='/' && s[i+1] == '*'))
		  {
		  	i++;  // Skip /
		  	i++;  // Skip *
		  }
		  else if ( (s[i]=='*' && s[i+1] == '/'))
		  {
		  	i++; // Skip *
		  	i++; // Skip /
		  }
           

          //#endif
		  if (s[i]=='\n')
		  {
		  	LineNumber++;
		  	
		  	//ColumnNumber=1;
		  }

       i++;
    }
    
  
   // Diagnostic messages.
   //#if(0)
  // std:: cout << "Comments parsing finished " << std::endl;
  // std::cout << "Total number of comments" << std::endl;
   //std::cout <<"Begin Comments: " << BeginComments << " End Comments: " << EndComments;
   if ((ErrorTracer::CheckforErrors()) && (CurrentPass==FIRST_PASS))
   {
   	    ErrorsFound=true;
		ErrorTracer::PrintErrors();
   }
   //#endif

   }
   /* Find common line numbers in c++ and c-style type of comments and elimiante them */
   CommentsProcessing::FindCommonlineNumbersinComments(CPPComments,CStyleComments);
   
   NumberofCommentLines=CPPComments.size()+CStyleComments.size();
   NumberofTotalLines=LineNumber-1;
   NumberofNonCommentLines=NumberofTotalLines-NumberofCommentLines;
   
   CurrentFile.NumberofCPPComments=CPPComments.size();
   CurrentFile.NumberofCStyleComments=CStyleComments.size();
   CurrentFile.NumberofCommentLines=NumberofCommentLines;
   CurrentFile.NumberofNonCommentLines=NumberofTotalLines-CurrentFile.NumberofCommentLines;
   CurrentFile.NumberofLines=NumberofTotalLines;
   CurrentFile.FileContent=s;
   
   /* Reset line number data */
   NumberofTotalLines=0;
   LineNumber=0;
   NumberofCommentLines=0;
   NumberofNonCommentLines=0;
   
   return ErrorsFound;
}

  

/*
Component Function: UINT32 CommentsProcessing::BeginCommentRead(std::vector <UINT8>& s, UINT32& i,  \
UINT32& BeginComments, UINT32& EndComments, UINT32& LineNumber,UINT32& CurrentPass,UINT32&  NumberofCStyleCommentLines, UINT32& NumberofDoubleSlashCommentLines, UINT32& LineNumberofEndComment)
Arguments: Vector s which contains program text,UINT32 i for position in the program, Number of Begin comments,
           Num of End comments, Line number )
Returns : Position of the program last read.
Description:
Read through a comment, parse text under comments and erase.
Call this function recursively each time a / * token is found.
This is a function to parse comments and nested comments.
Version : 0.2
Notes:
This function is working properly. 

To do:

*/
// Implement missing  */ token errors in single and nested comments. 
UINT32 CommentsProcessing::BeginCommentRead(std::vector <UINT8>& s, UINT32& i, UINT32& BeginComments,\
UINT32& EndComments, UINT32& LineNumber,UINT32& CurrentPass, UINT32&  NumberofCStyleCommentLines, UINT32& NumberofDoubleSlashCommentLines, UINT32 & LineNumberofEndComment)
{
    static UINT32 ReadPos=ZERO;
    static UINT32 NestedComments=ZERO;
    UINT32 EndCommentFound=false;
    static bool LineNumberinCStyleCommentChanged=false;
    
    // #if(0)
    //std::cout << "Erasing  /" << std::endl;
    // #endif

    if (CurrentPass==SECOND_PASS)
     {
         	s[i] = ' ';  // Erase /
     }
     i++;

    // #if(0)
    //std::cout << "Erasing  *" << std::endl;
    // #endif
    if (CurrentPass==SECOND_PASS)
     {
         	s[i] = ' ';  // Erase *

	 }
    i++;
    /* Code edit here */
    
    //#if(0)
    if (CurrentPass==SECOND_PASS)
    {
    	//#if(0)
    	if (LineNumberofEndComment != LineNumber)
    	{
    		NumberofCStyleCommentLines++;
    		CStyleComments.push_back(LineNumber);
		}
        LineNumberinCStyleCommentChanged=false;
        //#endif
       
    }
    //#endif
    
    UINT32 RecentBeginCommentLine=LineNumber;
    while((i<s.size()))
     // Read until */ token
    {
      
       // If a nested comment is found, start reading in that comment.
       if (s[i]=='/' && s[i+1]=='*')
       {
         // #if(0)
         // Diagnostic messages.
         //std::cout << "Begin comment /* detected. " << std::endl;
         BeginComments++;
         // #endif
         ReadPos=BeginCommentRead(s,i,BeginComments,EndComments,LineNumber,CurrentPass, NumberofCStyleCommentLines,NumberofDoubleSlashCommentLines,LineNumberofEndComment);
         // Use nested comment count to to erase until last */ in a nested comments set.
         NestedComments++;
         // #if(0)
         // Diagnostic messages.
         //std::cout << "Nested comments detected: " << NestedComments << std::endl;
         // #endif
         i=ReadPos;
       }
        // For now, we will assume there are no \n chars in cout " " strings
       if (s[i]=='\n' )
       {
           i++;
           LineNumber++;
           
               /* Code edit here */
           //#if(0)
           if (CurrentPass==SECOND_PASS)
           {
           	 NumberofCStyleCommentLines++;
           	 LineNumberinCStyleCommentChanged=true;
           	 if(LineNumberinCStyleCommentChanged)
           	 {
           	 	CStyleComments.push_back(LineNumber);
			 }
           }
           //#endif
           continue;
       }

       if ((s[i] =='*' && s[i+1] =='/'))
       {
         EndComments++;
         EndCommentFound=true;
         
             /* Code edit here */
           //#if(0) 
           if (CurrentPass==SECOND_PASS)
           {
           	 LineNumberofEndComment=LineNumber;
           	 
           }
           //#endif 
         break;
	   }
	    // #if(0)
        // Diagnostic message
        //std::cout<< "Erasing byte " << std::endl;
        //#endif
       if (CurrentPass==SECOND_PASS)
       {
         	s[i] = ' ';  // Erase all bytes before */ token

	   }
	   i++;
    }
    // Code for detecting missing */ token in a comment
    if (EndCommentFound==false)
    {
    	ErrorTracer::ErrorTracer(MISSING_COMMENT, RecentBeginCommentLine,1);
	}

    /* Detecting Comment ending and line number tracking in nested comments is to be implemented. */
    if ( NestedComments >0 )
    {

     while ( NestedComments >0 )
     {
       while((i<s.size()) && s[i] != '*' && s[i+1] != '/')
       {
        if (s[i]=='\n')
        {
           i++;
           LineNumber++;
           
        }
        else
         {
         // #if(0)
          // Diagnostic message.
          //std::cout<< " Erasing byte " << std::endl;
         // #endif
         if (CurrentPass==SECOND_PASS)
         {
         	s[i] = ' ';  // Erase all bytes before */ token
	     }
	     i++;
		 }
        }

    // #if(0)
     // Diagnostic messages.
     if (s[i]=='*' && s[i+1]=='/')
     {
        //std::cout <<"End nested comment */ detected " << std::endl;

	  if (CurrentPass==SECOND_PASS)
       {
       	    LineNumberofEndComment=LineNumber; 
         	s[i]=' '; // Erase *

	   }
	  i++;
     // #if(0)
      // Diagnostic message
     // std::cout << "Erasing  /" << std::endl;
     // #endif
      if (CurrentPass==SECOND_PASS)
       {
         	s[i]=' '; // Erase /

	   }
	   i++;
      EndCommentFound=true;
      
     }
     // #endif
      // Erase */ token within nested comments
     // #if (0)
      // Diagnostic message
      //std::cout << "Erasing  *" << std::endl;
     // #endif


      NestedComments--;

     }

   }
    else
    {
       //#if(0)
       // Diagnostic messages.
       //std::cout << "In outer nested comment block or in a single comment block"  << std::endl;
      // #endif

	while(i<s.size())
    // Read until */ token
     {
       // For now, we will assume there are no \n chars in cout " " strings
       if (s[i]=='\n' )
       {
           i++;
           continue;
       }

       else if (s[i] == '*' && s[i+1]== '/')
       {
       	    //std::cout <<"End comment */ detected " << std::endl;
       	    break;
       }


      // #if(0)
       // Diagnostic messages
       //std:: cout << "Erasing byte in outer nested or single inner block" << std::endl;
      // #endif

      if (CurrentPass==SECOND_PASS)
       {
         	s[i] = ' ';  // Erase all bytes before */ token

	   }
       i++;
     }

    //  #if(0)
     // Diagnostic messages.
     if (s[i]=='*' && s[i+1]=='/')
     {
        //std::cout <<"Outer End comment */ detected " << std::endl;

	   //std::cout << "Erasing  *" << std::endl;
	   if (CurrentPass==SECOND_PASS)
       {
       	    LineNumberofEndComment=LineNumber;
         	s[i]=' '; // Erase *

	   }
	   i++;
     //std::cout << "Erasing  /" << std::endl;
      if (CurrentPass==SECOND_PASS)
       {
         	s[i]=' '; // Erase /

	   }
	   i++;

     }
     // #endif
      //#if(0)
      // Diagnostic message.
      //std::cout << "Erasing  *" << std::endl;
     // #endif
    }

   return i;
}

/*
Component Function: void CommentsProcessing::FindCommonlineNumbersinComments(std::vector<UINT32> &CPPComments, std::vector <UINT32>& CStyleComments)
Arguments: Arguments which store CPP and C-style comments.
Returns : None
Description:
Finds common line numbers of C and C++ comments.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void CommentsProcessing::FindCommonlineNumbersinComments(std::vector<UINT32> &CPPComments, std::vector <UINT32>& CStyleComments)
{
	UINT32 index=0;
	UINT32 Count=0;
	UINT32 sizeofCPPCommentsVector =CPPComments.size();
	UINT32 sizeofCStyleCommentsVector =CStyleComments.size();
	std::vector <UINT32> ResultVector;
	BOOL MatchFound =false;
	for (index=0;index<sizeofCPPCommentsVector;index++)
	{
		for(Count=0;Count<sizeofCStyleCommentsVector;Count++)
		{
			if (CPPComments[index] == CStyleComments[Count])
			{
				MatchFound=true;
			}
		}
		if (!MatchFound)
		{
			ResultVector.push_back(CPPComments[index]);
		}
		MatchFound=false;			
	}
	
	CPPComments=ResultVector;
	
}

/*
Component Function: void CommentsProcessing::AddHeaderIntoList(struct FileData &CurrentFile)
Arguments: Add current file data into a vector of header names.
Returns : None
Description:
Addes a header file entry into headerslist.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void CommentsProcessing::AddHeaderIntoList(struct FileData &CurrentFile)
{
	UINT32 Size=ListofHeaderFiles.size();
	UINT32 Count=0;
	BOOL HeaderFound=false;
	for(Count=0;Count<Size && HeaderFound == false;Count++)
	{
		if (ListofHeaderFiles[Count].Filename == CurrentFile.Filename)
		{
			HeaderFound=true;
		}
	}
	if (!HeaderFound)
	{
		ListofHeaderFiles.push_back(CurrentFile);
	}
}