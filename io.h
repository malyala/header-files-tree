/*
Name :  Io.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
Description:
 * This module gets input from a file and writes output to a file.
Notes:
Bug and revision history;
Version 0.1 Initial version
 */

#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include <iostream>
#include <string>
#include "std_types.h"
#include "Error.h"

#if __cplusplus >= 201103L
/* namespace for IO operations */
namespace io 
{
    void GetInput(std::string &Filename);
    /*
    Read and display contents of a filename
    No need to change this function
    From cplusplus.com
     */

    void ReadFile(std::vector <UINT8> &s, std::string &Filename);

    /*
     This function should be commented in the main project.
     Diplay output with line numbers.
     */
    void DisplayOutput(const std::vector <UINT8> & s);
    /*
    Function Name : void WriteFile(std::string& Filename,std::string &DataFile)
    Description:
    Read and display contents of a filename
    From cplusplus.com.
    Arguments: Input file name 
    Preconditions: The function should write a file and add each character into a string.If there are errors, an error message should be displayed.
    Post conditions: Print error if unable to open file.
    Known issues:
    Unknown issues:
     */
    void WriteFile(std::string& Filename, std::string &DataFile);
}
#endif /* #if __cplusplus >= 201103L */
#endif
