
/*
Name :  CommentsProcessing.h
Author: Amit Malyala ,Copyright Amit Malyala 2016-Current.
Date : 14-11-16 02:08
Description:
This module reads comments and deletes them in a C++ source or header file.
*/

#ifndef COMMENTSPROCESSING_H
#define COMMENTSPROCESSING_H

#include "Preprocessor.h"
#include "io.h"
#include "error.h"


#if __cplusplus >= 201103L

/* Line data of each header or source file */
struct FileData
{
	UINT32 NumberofLines;
	UINT32 NumberofCommentLines;
	UINT32 NumberofNonCommentLines;
	UINT32 NumberofCPPComments;
	UINT32 NumberofCStyleComments;
	std::vector <UINT8> FileContent;
	std::string Filename;
};



/* Comment processing part of preprocessor */
namespace CommentsProcessing
{
/*
Component Function:void CommentsProcessing::InitCommentsProcessing(std::string & Filename)
Arguments: Reference to Filename
returns: None
Description:
Initialize Preprocessor and start to remove comments.
Bug and Version history :
         0.1 Initial version
*/
void InitCommentsProcessing(std::string & Filename);

// Remove comments from the program which is stored in a std::vector
UINT32 RemoveCppComments(std::vector <UINT8> & s,UINT32&  NumberofCommentLines, UINT32& NumberofNonCommentLines,UINT32& NumberofTotalLines);

/*
Component Function: UINT32 BeginCommentRead(std::vector <UINT8>& s, UINT32& i, UINT32& BeginComments,\
UINT32& EndComments, UINT32& LineNumber,UINT32& CurrentPass, UINT32&  NumberofCStyleCommentLines, UINT32& NumberofDoubleSlashCommentLines,UINT32& LineNumberofEndComment)
Arguments: Vector s which contains program text,UINT32 i for position in the program
returns : Position of the program last read.
Description:
Read through a comment, parse text under comments and erase.
Call this function recursively each time a / * token is found.
This is a function to parse comments and nested comments.
Version : 0.2
*/
UINT32 BeginCommentRead(std::vector <UINT8>& s, UINT32& i, UINT32& BeginComments,\
UINT32& EndComments, UINT32& LineNumber,UINT32& CurrentPass, UINT32&  NumberofCStyleCommentLines, UINT32& NumberofDoubleSlashCommentLines,UINT32& LineNumberofEndComment);

/*
Component Function: void FindCommonlineNumbersinComments(std::vector<UINT32> &CPPComments, std::vector <UINT32>& CStyleComments)
Arguments: Arguments which store CPP and C-style comments.
Returns : None
Description:
This function Deletes Line numbers in CPP Comments vector which are common in C-style comments vector.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void FindCommonlineNumbersinComments(std::vector<UINT32> &CPPComments, std::vector <UINT32>& CStyleComments);

/*
Component Function: void AddHeaderIntoList(struct FileData &CurrentFile)
Arguments: Add current file data into a vector of header names.
Returns : None
Description:
This function Deletes Line numbers in CPP Comments vector which are common in C-style comments vector.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void AddHeaderIntoList(struct FileData &CurrentFile);

}
#endif /* #if __cplusplus >= 201103L */
#endif 