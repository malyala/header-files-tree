/*
	Name: Tests.h
	Copyright: Amit Malyala 2016, All rights reserved
	Author: Amit Malyala
	Date: 25-10-16 23:47
	Description: 
	Module tests are defined here. The test cases would use actual functions and in some cases
	modified functions of all modules.
*/

#ifndef TESTS_ENABLED
#define TESTS_ENABLED



/* List of module tests */
#define DS_TEST 0x01
#define INPUT_TEST 0x02
#define LEXER_TEST 0x03
#define PARSER_TEST 0x04
#define AST_TEST 0x05
#define EVALUATOR_TEST 0x06

/* Include all headers for testing code */
#include "std_types.h"
#include "parser.h"
#include "DataStructures.h"
#include "iostream"
#include <iomanip>
#include "Modules.h"
#include "PreProcessor.h"
#include "cstring"

#if __cplusplus >= 201103L
namespace TestFramework
{

/*
Component Function: void TestDetectMissingOperandsandOperators(std::vector <Token_tag>& TokenStream)
Arguments:  Vector of tokens TokenStream
Returns: None
Version :
0.1 Initial version
Description:
Error detecting code should detect an error in a incorrect expression or subexpression such as
3+ or 6 7 or
Any Operator should have a following token which is a numeric type.
Any operand should not have a preceding token which is also a operand.
Notes:
Function being designed.
 */
void TestDetectMissingOperandsandOperators(std::vector <Token_tag>& TokenStream);
/*
Component Function: void TestConstructBinaryExpressionTree(void)
Arguments: None
Returns : None
Description:
In this function we convert a infix expression to postfix.Use postfix notation string to construct binary expression tree.
Initially the parser will construct using a postfix notation string, after this is done, the parser will use lexer output stream to construct AST.
The important thing here is to enable expression evaluation in the midst of statements and compound statements which the AST will facilitate.
The AST will evaluate expressions in macro definition.A postfix evaluator function is only for expression of arithmetic operands and operators.
Version and bug history:
0.1 Initial version.
Notes:
Function being designed.
This function creates nodes and then forms a tree using a stack of Tnode types.
This function would select the position of each node being inserted into the expression tree.  Check if a  recursive function can be
written for constructing the tree.
This function needs to be designed from the beginning.
Node creation works fine.
Check node insertion.
*/
 
void TestConstructBinaryExpressionTree(void);

/*
Component Function: void TestTreeConstructManually(void) 
Arguments: None
Returns : None
Description:
Construct a expression tree manually and evaluate it.
Version and bug history:
0.1 Initial version.
Notes:
Function being designed.
*/

void TestTreeConstructManually(void);
/*
Arguments:  Multiplication factors for creating float and double variables.
Returns: None
Version :
0.1 Initial version
Description:
 */
void TestMultipliers(void);

/*
Component Function: void PrintHeaders(const std::vector <std::string> &str)
Arguments: vector of strings
returns : None
Description
Print words in a header
Notes:
Function being designed.
Version and bug history : 
0.1 Initial version.
 */
void PrintHeaders(const std::vector <std::string> &str);


/*
 Component Function: void TestPrintTokenStrings(void)
 Arguments:  None
 returns: None
 Description:
 Prints contents of strings. 
 Version : 0.1
 Notes:
 */
void TestPrintTokenStrings(void);

/*
 Component Function: void TestPreprocessor(void)
 Arguments:  
 returns: None
 Description:
 Prints contents of strings. 
 Version : 0.1
 Notes:
 */
void TestVectortoString(void);

/* Print vector */
void PrintVector(std::vector <UINT32> &VectorName);

/*
Component Function:  void InitLexer(void))
Arguments:  Filename
returns: None
Description:
Initialize Lexer module.
Version : 0.1
 */
void InitLexer(void);

/*
 Component Function: ClearToken(Token_tag &Token)
 Arguments:  None
 returns: None
 Description:
 Clears a token to read next token
 Version : 0.1
 */
void ClearToken(Token_tag &Token);

/*
Component Function:  void GenerateLexerTokens(std::vector <Token_tag>& TokenStream,struct HeaderData &CurrentHeader)
Arguments: Tokenstream vector
returns: None
Description:
Generates Lexer tokens from a input string
Version : 0.1
Notes:

    Processing Headers in source files.
    -------------------------------------
    #include "io.h"
    #include "Lexer.h"

    This function should generate  tokens for source or header files with headers as:

    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER io.h
    Token - ENDHEADER
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER Lexer.h
    Token - ENDHEADER
    Token - NEWLINE

To do:

*/
void GenerateLexerTokens(std::vector <Token_tag>& TokenStream,struct HeaderData &CurrentHeader);

/*
 void DetectAllOtherTokens(std::string &CharString, UINT32 &index, Token_tag &Token, UINT32&  NumberofCharacterssinFile, UINT32& LineNumber,UINT32& ColumnNumber)
 Arguments:
 returns: None
 Description:
 Detects numeric data such as int,float, double, short, ushort,long,ulong, long double and strings.
 Version : 0.1


    Processing Headers in File.cpp
    ---------------------
    
    #include <io.h>
    #include "Lexer.h"
        
    This function should generate  tokens for source files with headers as.
    
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER io.h
    Token - ENDHEADER
    Token - NEWLINE
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER Lexer.h
    Token - ENDHEADER
    Token - NEWLINE

 Notes:
 Function being designed.

 Known issues:
 None

 Unknown issues:
 None
 */
void DetectAllOtherTokens(std::string &CharString, UINT32 &index, Token_tag &Token, UINT32&  NumberofCharacterssinFile, UINT32& LineNumber,UINT32& ColumnNumber);

/*
 Component Function: void DisplayTokenList(std::vector <Token_tag>& Tokenstream)
 Arguments:  Tokenstream vector
 returns: None
 Description:
 Displays tokens in tokenstream.
 Version : 0.1
 Notes:
 */
void DisplayTokenList(std::vector <Token_tag>& Tokenstream);

/*
Component Function: void InitParser(void)
Arguments: None
Returns : None
Description:
Initializes parser.Gathers list of header names.

Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void InitParser(void);


/*
Component Function: void ParseTokenstream(std::vector <Token_tag> &TokenStream)
Arguments: Token stream from lexer output
Returns: None
Version : 0.1 Initial version.
Description:
The function would take vector of tokens as a argument and push header names into file stacks.

Notes:
To do:

 */
void ParseTokenstream(std::vector <Token_tag> &TokenStream);
/*
Component Function: void DetectHeaders(void)
Arguments: None
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void DetectHeaders(void);	
/*
Component Function: void HeadersProcessing::EvaluateHeaderStack(void)
Arguments: None
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void EvaluateHeaderStack(void);

/*
Component Function: void EvaluateIntermediateStack(void)
Arguments: void
Returns : None
Description:
This function evaluates all headers in intermediate stack.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void EvaluateIntermediateStack(void);
/*
Component Function: void HeadersProcessing::ClearHeaderData(HeaderData &CurrentHeader)
Arguments: Current header
Returns : None
Description:
Clears Header data.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void ClearHeaderData(HeaderData &CurrentHeader);


/*
Component Function: void SetHeaderData(HeaderData& CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
Arguments: Header container and Header name name
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void  SetHeaderData(HeaderData& CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed);

/*
Component Function: BOOL DetectSameIndentationofHeaders(void)
Arguments: Header container and Header name name
Returns : None
Description:
This function uses intermediateHeaderstack and determines if it contains headers with same indentation, if yes, then 
a true is returned. If all headers have unique indentation, then a false is returned.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
BOOL DetectSameIndentationofHeaders(void);

/*
Component Function: void PrintCurrentHeader(HeaderData& CurrentHeader)
Arguments: Header container and Header name name
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void PrintCurrentHeader(HeaderData& CurrentHeader);

/*
Component Function: void CorrectIndentationLevelsinHeaderStack(const std::vector <HeaderData> &pS)
Arguments:  Reference to stack.
Returns: true or not
Description:  Adjusts indentation levels in header stack.
Version : 0.1
Notes:
 */
void CorrectIndentationLevelsinHeaderStack(const std::vector <HeaderData> &pS);

/*
Component Function: void DisplayHeaderStack(const std::vector <HeaderData> &pS)
Arguments:  Reference to stack.
Returns: None
Description:  Displays Header stack
Version : 0.1
Notes:
 */
void DisplayHeaderStack(const std::vector <HeaderData> &pS);

}

#endif /* #if __cplusplus >= 201103L */
#endif 
