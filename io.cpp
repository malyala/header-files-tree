/*
Name :  Input.cpp
Author: Amit Malyala , Copyright Amit Malyala 2016-Current.All rights reserved.
Date : 20-06-2016
Description:
This module gets input from a source or header file.
Notes:

Version history:

0.1 Initial version
0.2 Implemented a end of input flag
0.3 Reads input from a text file on disk.
 */

#include "Io.h"
#include "Lexer.h"
#include <fstream>

/* Program string */
std::vector <UINT8> s;

/* Get input from user. */
void io::GetInput(std::string &Filename)
{
    s.clear();
    //std::cout << "Reading source file: " << Filename << std::endl;
    ReadFile(s, Filename);
    //DisplayOutput(s);
}

/*
 Component Function: void io::ReadFile(std::vector <UINT8> &s, std::string &Filename)
 Arguments:  Vector to process input and input file name.
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
 From cplusplus.com
 */
void io::ReadFile(std::vector <UINT8> &s, std::string &Filename)
{
    //std::string line;
    BOOL EndofInput = false;
    std::ifstream myfile(Filename);
    SINT8 c;
    if (myfile.is_open())
    {
        while (myfile.get(c) && EndofInput == false) // For reading bytes
        {
            /*
              Read input until EOF
            */
            if (c == EOF)
            {
                EndofInput = true;
            } else
            {
                s.push_back(c);
            }

        }
        myfile.close();
    } else
    {
        ErrorTracer::ErrorTracer(UNABLE_TO_READ_FILE,0,0);
    }
}

/*
 This function should be commented in the main project.
 Diplay output with line numbers. Print line numbers.
 */
void io::DisplayOutput(const std::vector <UINT8> & s)
{
    UINT32 i = 0;
    UINT32 Linenumber = 1;
    BOOL Linenumberprinted = false;
    while (i < s.size())
    {
        if (Linenumberprinted == false)
        {
            std::cout << Linenumber << ": ";
            Linenumberprinted = true;
        }
        std::cout << s[i];    
        
        if (s[i] == '\n')
        {
            Linenumber++;
            Linenumberprinted = false;
        }
        
        i++;
    }
    std::cout << std::endl;
}


/*
Function Name : void io::WriteFile(std::string& Filename,std::string &DataFile)
Description:
Read and display contents of a filename
From cplusplus.com.
Arguments: Input file name 
Preconditions: The function should write a file and add each character into a string.If there are errors, an error message should be displayed.
Post conditions: Print error if unable to open file.
Known issues:

Unknown issues:
*/

void io::WriteFile(std::string& Filename,std::string &DataFile)
{
  std::ofstream myfile (Filename);
  
  if (myfile.is_open())
  {
   	myfile.write(DataFile.data(),DataFile.size());
    myfile.close();
  }
  else 
  {
  	ErrorTracer::ErrorTracer(UNABLE_TO_WRITE_FILE,0,0);
  }
  
}

