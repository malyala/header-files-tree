/*
Name :  Lexer.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
Description:
The lexer module extracts words,numbers and symbols from the program.
Notes:
Bug and version history:
Version 0.1 Initial version
0.2 Added constants for macro, macro args
 */

#ifndef LEXER_H
#define LEXER_H


#include <vector>
#include <string>
#include "Std_types.h"
#include "PreProcessor.h"

// For displaying debugging information
#define LEXER_DEVELOPMENT_DEBUGGING_ENABLED 0x01

//#if(0)
/* Define Token types */
/* The lexer would detect each of these tokens in the C++ program. Each token read is then added into a vector of tokens */

//#if __cplusplus >= 201103L
/* Use enums to list all tokens and to detect token type */
enum TokenType
{
    NUMERIC= 2,
    ENDOFLINE = '\\',
    PLUS = '+',
    MINUS = '-',
    MUL = '*',
    DIV = '/',
    EXP = '^',
    MODULO='%',
	SPACE = ' ',
    DOT = '.',
    NEWLINE = '\n',
    LEFTPAREN = '(',
    RIGHTPAREN = ')',
    UNARYMINUS = '_',
    ARGSEP = ',',
    HASH = '#',
    SEMICOLON = ';',
    LESSTHAN = '<',
    GREATERTHAN = '>',
    NOT = '!',
    OR = '|',
    AND = '&',
    EQUALTO = '=',
    COMPLEMENT = '~',
    QUOTE = '"',
    PREPROCLESSTHAN = '<',
    PREPROCGREATERTHAN = '>',
    LEFTBRACE = '{',
    RIGHTBRACE = '}',
    LEFTBRACKET = '[',
    RIGHTBRACKET = ']',
    TILDA ='`',
    AT = '@',
    UNDERSCORE='_'
    
};

/* Type Numeric Tokens */
#define LONGDOUBLETYPE 0
#define DOUBLETYPE 1
#define FLOATTYPE 2
#define UINTTYPE 3
#define INTTYPE 4
#define LONGINTTYPE 5
#define LONGLONGINTTYPE 6
#define ULONGINTTYPE 7
#define SHORTTYPE 8
#define USHORTTYPE 9
#define UCHARTYPE 10
#define CHARTYPE 11
#define BOOLTYPE 12
#define CSTRINGTYPE 13
#define NOTADATATYPE 14

/*
List operator tokens as one group */
/*
Type of Token data type used in addition to token information if applicable
 */
// For operators or other symbols

/* Other tokens */
#define HASHTYPE 15
#define LEFTPARENTHESIS 16
#define RIGHTPARENTHESIS 17
#define ENDOFINPUT 18
#define NOT_A_NUMBER 19
#define NOTOP 20
#define NEWLINETYPE 21

/* Macro specific constants */

#define ARGSEPARATOR 22
#define WHITESPACE 23
#define IDENT 24 /* Uninitialized ident */
#define DEFINE 25
#define MACRO 26
#define INCLUDE 27
#define HEADER 28
#define QUOTETYPE 29
#define NAME 30
#define SYMBOLLESSTHAN 31
#define SYMBOLGREATERTHAN 32
#define ENDOFLINETYPE 33
#define SYMBOLSLASH 34
#define INCLUDENEXT 35

#define STRINGDEFINE 200
#define STRINGIDENTIFIER  201
#define STRINGMACRO  202
#define NOTSTRING 207
#define STRINGINCLUDE 208
#define STRINGHEADER 209
#define STRINGNAME 282
#define STRINGINCLUDENEXT 283

#define STARTOFMACRODEFINITION 210
#define STARTOFMACROEXPRESSION 211
#define STARTOFMACRODECLARATION 212
#define ENDOFMACRODECLARATION 213

/* Some preprocessor tokens */
#define PREPROCESSORHEADER 214
#define PREPROCESSORIF 215
#define PREPROCESSORIFDEF 216
#define PREPROCESSORIFNDEF 217
#define PREPROCESSORELSE 218
#define PREPROCESSORENDIF 219
#define PREPROCESSORPRAGMA 220
#define BEGINHEADER 221
#define ENDHEADER 222

/* List of operators */
#define OPPLUS 250
#define OPMINUS 251
#define OPMUL 252
#define OPDIV 253
#define OPMODULUS 254
#define OPEQUALTO 255
#define OPUNARYMINUS 256
#define OPUNARYPLUS 257
#define OPPOWEXPONENT 258
#define OPSIN 259
#define OPCOS 260
#define OPTAN 261
#define OPCOSEC 262
#define OPSEC 263
#define OPCOT 264
#define OPSQRT 265
#define OPLOG 266
#define OPLESSTHAN 267
#define OPGREATERTHAN 268
#define OPLEFTBITSHIFT 269
#define OPRIGHTBITSHIFT 270
#define OPLOGICALNOT 271
#define OPBITWISEOR 272
#define OPBITWISEAND 273
#define OPLOGICALAND 274
#define OPLOGICALOR 275
#define OPLESSTHANEQUALTO 276
#define OPGREATERTHANEQUALTO 277
#define OPLOGICALISEQUALTO 278
#define OPLOGICALISNOTEQUALTO 279
#define OPBITWISEXOR 280
#define OPCOMPLEMENT 281

//#endif /* #if __cplusplus >= 201103L */
/* Add each token_tag into a vector as each character or group of characters are read from input stream */
/* To be defined and modified */

typedef struct Token_tag
{
    /* Type representation of the tokens for a statement such as 3+5*2; */
    /* Strings in this paramater can be  "INTTYPE,USHORTTYPE,UINTTYPE,SHORTTYPE,FLOATTYPE,DOUBLETYPE,LONGINTTYPE,ULONGINTTYPE,
	 LONGDOUBLETYPE, OPERATOR,IDENT,LEFTPAREN, RIGHTPAREN */
    const SINT8 *Meta;
    /* Line number of each token */
    UINT32 LineNumber;
    /* Column number of each token */
    UINT32 ColumnNumber;
    /* Type of data stored in a token if there are special tokens which contain data */
    UINT32 TokenDataType;
    /* This parameter is used to tag any token with a macro definition */
    /* The RPN stack will be run after its formed and identifiers are tagged with their scope, which is definition of a macro in which
    identifiers are looked at */
    const SINT8* Scope;
    /* Union of this type will store data of type unsigned int , int,long int ,
	ulong int, double, float, long double and any char string */
	Variant Data;
} Token_tag;


#if __cplusplus >= 201103L
/* Functions in Lexer module */
namespace Lexer
{

/*
Component Function:  void InitLexer(void)
Arguments:  Header meta
returns: None
Description:
Initializes Lexer module.
Version : 0.1
*/
void InitLexer(void);


/*
Component Function:  void GenerateLexerTokens(std::vector <Token_tag>& TokenStream)
Arguments:  Tokenstream vector
returns: None
Description:
Generates Lexer tokens from a input string
Version : 0.1
*/
void GenerateLexerTokens(std::vector <Token_tag>& TokenStream);


    /*
     Component Function: void DetectAllOtherTokens(std::string &OtherString, UINT32 &index, Token_tag &Token,UINT32&  NumberofCharacterssinFile,UINT32& LineNumber,UINT32& ColumnNumber)
     Arguments:
     returns: None
     Description:
     Detects numeric data such as int,float, double, short, ushort,long,ulong, long double
     Version : 0.1
     */
    void DetectAllOtherTokens(std::string &OtherString, UINT32 &index, Token_tag &Token, UINT32&  NumberofCharacterssinFile,UINT32& LineNumber, UINT32& ColumnNumber);

    #if defined (LEXER_DEVELOPMENT_DEBUGGING_ENABLED)
    /*
     Component Function: static void DisplayTokenList(std::vector <Token_tag>& Tokenstream)
     Arguments:  None
     returns: None
     Description:
     Clears at token to read next token
     Version : 0.1
     */
    void DisplayTokenList(std::vector <Token_tag>& Tokenstream);
    /* Change all the following functions */
    /*
    Component Function: void PrintToken(const Token_tag& Token)
    Arguments:  Reference to token
    Returns: true or not
    Description:  Displays data in token.
    Version : 0.1
    Notes:
    This function would be used while construction of AST.
     */
    void PrintToken(const Token_tag& Token);
    #endif // #if defined (LEXER_DEVELOPMENT_DEBUGGING_ENABLED)


    /*
     Component Function: static void ClearToken(Token_tag &Token)
     Arguments:  None
     returns: None
     Description:
     Clears at token to read next token
     Version : 0.1
     */
    void ClearToken(Token_tag &Token);

    /*
     Component Function: void MemFreeTokenList(std::vector <Token_tag>& Tokenstream)
     Arguments:  Tokenstream vector
     returns: None
     Description:
     Clears memory allocated for tokens in tokenstream.
     Version : 0.1
     Notes:
     */
    void MemFreeTokenList(std::vector <Token_tag>& Tokenstream);



    /*
    Component Function: void PrintRawToken(const Token_tag& Token)
    Arguments:  Reference to token
    Returns: true or not
    Description:  Displays data in token.
    Version : 0.1
    Notes:
    This function would be used while construction of AST.
     */
    void PrintRawToken(const Token_tag& Token);
}

#endif /* #if __cplusplus >= 201103L */
#endif

