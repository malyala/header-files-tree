/*
        Name: TestFramework.cpp
        Copyright: Amit Malyala, 2016-Current. All rights reserved.
        Author: Amit Malyala
        Date: 26-10-16 15:58
        Description:
        This module executes module and coverage tests for preprocessor.
        Test code should be written in this module.
 */


#include "Testframework.h"
#include "math.h"
#include "CommentsProcessing.h"
#include "TextAnalysis.h"
#include "Stack.h"

/* For accessing Lexer token stream */
extern std::vector <Token_tag> TokenList;

/* Define a vector for Parenthesis */
extern std::vector <Token_tag> ParenthesisList;

/* Double slash comment lines */
extern std::vector <UINT32> CPPComments;
/* C-style comments */
extern std::vector <UINT32> CStyleComments;

/* Words of a header */
extern std::vector <std::string> WordList;

/* Processing original file or not */
extern BOOL OriginalFileProcessed;

/* List of header files in a original source file at root level */
extern std::vector <std::string> HeaderListinOriginalSource;

/* A Stack of headers */
extern std::vector <HeaderData> HeaderStack;


/* Miscellaneous data of each file processed */
extern struct FileData CurrentFile;
// vector::pop_back
#include <iostream>
#include <vector>

#if(0)

SINT32 main(void)
{
//#if(0)

    std::string Filename = "Preprocessor.cpp";
    OriginalFileProcessed = true;
    HeaderData aHeader;
    HeadersProcessing::ClearHeaderData(aHeader);
    // Read also from a file by modifying the lines below.

    CommentsProcessing::InitCommentsProcessing(Filename);
    if (ErrorTracer::CheckforErrors() == NO_ERRORS)
    {
        Lexer::InitLexer();
        //Lexer::DisplayTokenList(TokenList);
        std::cout << "Before Initparser" << std::endl;
        Parser::InitParser();
        std::cout << "After Initparser" << std::endl;
    } else
    {
        ErrorTracer::PrintErrors();
    }
    Stack::ReverseOriginalSourceStack();
    Stack::PrintOriginalSourceStack();
    std::cout << "File Data:" << std::endl;
    std::cout << "Number of C-style comment lines " << CurrentFile.NumberofCStyleComments << std::endl;
    std::cout << "Number of C++ comment lines " << CurrentFile.NumberofCPPComments << std::endl;
    std::cout << "Total number of comment lines " << CurrentFile.NumberofCommentLines << std::endl;
    std::cout << "Number of non comment lines " << CurrentFile.NumberofNonCommentLines << std::endl;
    std::cout << "Number of total lines " << CurrentFile.NumberofLines << std::endl;
    //#endif
    //TestFramework::DetectHeaders();
    TextAnalysis::PrintHeaders();

    return 0;
}
#endif

/*
 Component Function: void TestFramework::TestPrintTokenStrings(void)
 Arguments:  None
 returns: None
 Description:
 Prints contents of strings. 
 Version : 0.1
 Notes:
 */
void TestFramework::TestPrintTokenStrings(void)
{
    //std::cout << "Printing all token strings " << std::endl;
    UINT32 index = 0;
    for (index = 0; index < TokenList.size(); index++)
    {
        std::cout << TokenList[index].Data.u.cString << std::endl;
    }
}

/*
Component Function: void TestFramework::PrintHeaders(const std::vector <std::string> &str)
Arguments: vector of strings
returns : None
Description
Print words in a header
Notes:
Function being designed.
Version and bug history : 
0.1 Initial version.
 */
void TestFramework::PrintHeaders(const std::vector <std::string> &str)
{
    UINT32 index = 0;
    UINT32 size = str.size();
    for (index = 0; index < size; index++)
    {
        std::cout << WordList[index] << std::endl;
    }
}

/*
 Component Function: void TestFramework::TestPreprocessor(void)
 Arguments:  
 returns: None
 Description:
 Prints contents of strings. 
 Version : 0.1
 Notes:
 */
void TestFramework::TestVectortoString(void)
{
    std::vector <UINT8> OriginalVector;
    SINT8 c = ' ';
    std::string name = "file.txt";
    std::string newString;
    for (c = 97; c <= 122; c++)
    {
        OriginalVector.push_back(c);
    }

    DS::VectortoString(newString, OriginalVector);
    io::WriteFile(name, newString);
}

/* Print vector */
void TestFramework::PrintVector(std::vector <UINT32> &VectorName)
{
    UINT32 size = VectorName.size();
    UINT32 index = 0;
    for (index = 0; index < size; index++)
    {
        std::cout << VectorName[index] << std::endl;
    }
}

/*
Component Function:  void TestFramework::InitLexer(void))
Arguments:  Filename
returns: None
Description:
Initialize Lexer module.
Version : 0.1
 */
void TestFramework::InitLexer(void)
{
    TokenList.clear();
    ParenthesisList.clear();
    Lexer::GenerateLexerTokens(TokenList);
}

/*
 Component Function: static void TestFramework::ClearToken(Token_tag &Token)
 Arguments:  None
 returns: None
 Description:
 Clears a token to read next token
 Version : 0.1
 */
void TestFramework::ClearToken(Token_tag &Token)
{
    Token.Meta = "NONE";
    Token.LineNumber = ZERO;
    Token.ColumnNumber = ZERO;
    Token.TokenDataType = NOTADATATYPE;
    Token.Data.u.cString = "NONE";
}

/*
Component Function:  void TestFramework::GenerateLexerTokens(std::vector <Token_tag>& TokenStream,struct HeaderData &CurrentHeader)
Arguments: Tokenstream vector
returns: None
Description:
Test case checks design which Generates Lexer tokens from a input string
Version : 0.1
Notes:

    Processing Headers in source files.
    -------------------------------------
    #include "io.h"
    #include "Lexer.h"

    This function should generate  tokens for source or header files with headers as:

    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER io.h
    Token - ENDHEADER
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER Lexer.h
    Token - ENDHEADER
    Token - NEWLINE

To do:
  Detect strings for the above lines as:
  include
  io.h
  include
  lexer.h
 
  Lines as these should be detected by Lexer as strings and IO.h being a string name  if they are in the middle or end of a source or header file.
  #ifndef IO_H
  #include "IO.h"
  #endif

 */
void TestFramework::GenerateLexerTokens(std::vector <Token_tag>& TokenStream, struct HeaderData &CurrentHeader)
{
    UINT32 NumberofCharacterssinFile = CurrentHeader.HeaderContent.size();
    UINT32 index = ZERO;
    static std::string NumericString;
    static std::string CharString;
    UINT32 ErrorsFound = false;
    Token_tag Token;
    UINT32 LineNumber = ONE;
    UINT32 ColumnNumber = ONE;
    //UINT32 NumericType = NOTADATATYPE;
    /*  NumberofCharacterssinFile variable should be changed to NumberofCharacters in file */
    if (NumberofCharacterssinFile)
    {
        while (index < NumberofCharacterssinFile && ErrorsFound == false)
        {
            Lexer::ClearToken(Token);
            switch (CurrentHeader.HeaderContent[index])
            {
                case SPACE:
                    Token.Meta = "WHITESPACE";
                    Token.TokenDataType = WHITESPACE;
                    Token.Data.u.cString = " ";
                    Token.ColumnNumber = ColumnNumber;
                    Token.LineNumber = LineNumber;
                    TokenStream.push_back(Token);
                    break;
                case HASH:
                    Token.Meta = "HASH";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = HASHTYPE;
                    Token.Data.u.cString = "#";
                    TokenList.push_back(Token);
                    break;
                case QUOTE:
                    Token.Meta = "QUOTE";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = QUOTETYPE;
                    Token.Data.u.cString = "\"";
                    TokenList.push_back(Token);
                    break;
                case NEWLINE:
                    Token.Meta = "NEWLINE";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = NEWLINETYPE;
                    Token.Data.u.cString = "\n";
                    TokenList.push_back(Token);
                    LineNumber++;
                    ColumnNumber = 1;
                    break;
                case LESSTHAN:
                    Token.Meta = "LESSTHAN";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = OPLESSTHAN;
                    Token.Data.u.cString = "<";
                    TokenList.push_back(Token);
                    break;
                case GREATERTHAN:
                    Token.Meta = "GREATERTHAN";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = OPGREATERTHAN;
                    Token.Data.u.cString = ">";
                    TokenList.push_back(Token);
                    break;

                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case '.':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '_':

                    /* Write code to detect macro tokens and #defines such as #define IDENT 1 , #define MACRO(a,b) (a+b) tokens  */
                    DetectAllOtherTokens(CharString, index, Token, NumberofCharacterssinFile, LineNumber, ColumnNumber);
                    break;
                case ENDOFLINE:
                    break;

                    break;
                default:
                    /*
                    Scan all characters.
                     */
                    break;
            }
            index++;
            ColumnNumber++;
        }
    } else
    {
        ErrorTracer::ErrorTracer(EMPTY_EXPRESSION, 0, 0);
    }

}

/*
 void TestFramework::DetectAllOtherTokens(std::string &CharString, UINT32 &index, Token_tag &Token, UINT32&  NumberofCharacterssinFile, UINT32& LineNumber,UINT32& ColumnNumber)
 Arguments:
 returns: None
 Description:
 Detects numeric data such as int,float, double, short, ushort,long,ulong, long double and strings.
 Version : 0.1


    Processing Headers in File.cpp
    ---------------------
    
    #include <io.h>
    #include "Lexer.h"
        
    This function should generate  tokens for source files with headers as.
    
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER io.h
    Token - ENDHEADER
    Token - NEWLINE
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER Lexer.h
    Token - ENDHEADER
    Token - NEWLINE

 Notes:
 Function being designed.
To do:

 Detect strings for the above lines as:
  include
  io.h
  include
  lexer.h
 
 Lines as these should be detected by Lexer as strings and IO.h being a string name when they are placed at middle or end of a source or header file.
 #ifndef IO_H 
 #include "IO.h"
 #endif
  CharString should finish reading a string as soon as it sees a char which is not a word char and DS::IsNotWordChar(str) should be true for such char.
 
 Known issues:
 
 
 Unknown issues:
 None

 */
void TestFramework::DetectAllOtherTokens(std::string &CharString, UINT32 &index, Token_tag &Token, UINT32& NumberofCharacterssinFile, UINT32& LineNumber, UINT32& ColumnNumber)
{
    UINT32 StringType = NOTSTRING;
    //std::cout <<"In  DetectAllOtherTokens() function " << std::endl;
    // Code during definition.

    CharString += s[index];
    /* Lookahead and see if the next char follwing a alphanumeric string is a symbol in list of NotWordChar Function. */
    if (((index + 1) <= NumberofCharacterssinFile) && (!DS::IsHeaderCharacter(s[index + 1])))
    {

        //std::cout << "Char string detected is " << CharString << std::endl;
        StringType = DS::DetectStringType(CharString);
        //StringType = DetectStringType(CharString.c_str(), index, s);
        // check this line
        if (StringType == STRINGDEFINE && (s[index + 1] == SPACE || s[index + 1] == '\0'))
        {
            //std::cout << "define string detected in DetectAllotherTokens() is " << CharString << std::endl;
            //std::cout << "index is at in DetectAllotherTokens() after detecting define " << index << std::endl;
            Token.Meta = "DEFINE";
            Token.LineNumber = LineNumber;
            Token.ColumnNumber = ColumnNumber;
            Token.TokenDataType = DEFINE;
            //Token.Data.u.cString = strdup(CharString.c_str());
            Token.Data.u.cString = "define";
            TokenList.push_back(Token);
            //std::cout <<"Column " << Column << std::endl;
            //Lexer::ClearToken(Token);
            CharString = "";
        } else if (StringType == STRINGINCLUDE && (s[index + 1] == SPACE))
        {
            //std::cout << "define string detected in DetectAllotherTokens() is " << CharString << std::endl;
            //std::cout << "index is at in DetectAllotherTokens() after detecting define " << index << std::endl;
            Token.Meta = "INCLUDE";
            Token.LineNumber = LineNumber;
            Token.ColumnNumber = ColumnNumber;
            Token.TokenDataType = INCLUDE;
            //Token.Data.u.cString = strdup(CharString.c_str());
            Token.Data.u.cString = "include";
            TokenList.push_back(Token);
            //std::cout <<"Column " << Column << std::endl;
            //Lexer::ClearToken(Token);
            CharString = "";
        } else if (StringType == STRINGNAME)
        {
            //std::cout << "index is at in DetectAllotherTokens()after detecting IDENT " << index << std::endl;
            Token.Meta = "NAME";
            Token.LineNumber = LineNumber;
            Token.ColumnNumber = ColumnNumber;
            Token.TokenDataType = NAME;
            //std::cout << "IDENT String detected in DetectAllotherTokens() is " << CharString << std::endl;
            Token.Data.u.cString = (strdup(CharString.c_str()));
            //std::cout << "IDENT String detected in DetectAllotherTokens() after copying to token is " << Token.Data.u.cString << std::endl;
            TokenList.push_back(Token);
            //std::cout << "Printing token " << std::endl;
            //PrintToken(Token);
            //std::cout <<"Column " << Column << std::endl;
            //Lexer::ClearToken(Token);
            CharString = "";
        }

        if (DS::IsNotWordChar(s[index + 1]) || s[index + 1] == ' ')
        {
            CharString = "";
        }
    }

}

/*
 Component Function: void TestFramework::DisplayTokenList(std::vector <Token_tag>& Tokenstream)
 Arguments:  Tokenstream vector
 returns: None
 Description:
 Displays tokens in tokenstream.
 Version : 0.1
 Notes:
 */
void TestFramework::DisplayTokenList(std::vector <Token_tag>& Tokenstream)
{
    UINT32 Length = Tokenstream.size();
    UINT32 index = ZERO;
    std::string outputstring;
    //std::ostringstream ss;
    //BOOL ErrorsFound = CheckforErrors();
    BOOL ErrorsFound = false;
    if (ErrorsFound == false)
    {
        while (index < Length)
        {
            switch (Tokenstream[index].TokenDataType)
            {
                case LEFTPARENTHESIS:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token Left parenthesis " << Tokenstream[index].Data.u.cString << std::endl;
                    ;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    ;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    ;
                    break;
                case RIGHTPARENTHESIS:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token Right parenthesis " << Tokenstream[index].Data.u.cString << std::endl;
                    ;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    ;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case OPPLUS:
                case OPMINUS:
                case OPDIV:
                case OPMODULUS:
                case OPMUL:
                case OPPOWEXPONENT:
                case OPSIN:
                case OPCOS:
                case OPTAN:
                case OPCOSEC:
                case OPSEC:
                case OPCOT:
                case OPSQRT:
                case OPLOG:
                case OPLESSTHAN:
                case OPGREATERTHAN:
                case OPLEFTBITSHIFT:
                case OPRIGHTBITSHIFT:
                case OPLOGICALNOT:
                case OPBITWISEAND:
                case OPBITWISEOR:
                case OPLOGICALAND:
                case OPLOGICALOR:
                case OPLESSTHANEQUALTO:
                case OPGREATERTHANEQUALTO:
                case OPLOGICALISEQUALTO:
                case OPLOGICALISNOTEQUALTO:
                case OPBITWISEXOR:
                case OPCOMPLEMENT:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token operator " << Tokenstream[index].Data.u.cString << std::endl;
                    ;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    ;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    ;
                    break;
                case ARGSEPARATOR:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token Arg separator " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case QUOTETYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token QUOTE " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case DEFINE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token DEFINE " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case IDENT:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token Ident " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case INCLUDE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;

                case MACRO:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token MACRO " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case HASHTYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token HASHTYPE " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case BEGINHEADER:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token Begin Header " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case HEADER:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token Header " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case QUOTE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token quote " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case ENDHEADER:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token End Header " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case STARTOFMACROEXPRESSION:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token STARTOFMACROEXPRESSION " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case STARTOFMACRODECLARATION:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token STARTOFMACRODECALRATION " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case STARTOFMACRODEFINITION:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "Token STARTOFMACRODEFINITION " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
#if(0)        
                case WHITESPACE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token WHITESPACE " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
#endif  
                case ENDOFMACRODECLARATION:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    ;
                    std::cout << "End of MacroDeclaration " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case SHORTTYPE:
                    std::cout << "Token Meta  " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token type short " << Tokenstream[index].Data.u.shortValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case USHORTTYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token type unsigned short " << Tokenstream[index].Data.u.ushortValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case INTTYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token type int " << Tokenstream[index].Data.u.intValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case UINTTYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token type unsigned int " << Tokenstream[index].Data.u.uintValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case LONGINTTYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token type long int " << Tokenstream[index].Data.u.longValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case ULONGINTTYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token type unsigned long int " << Tokenstream[index].Data.u.ulongValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case FLOATTYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    //ss << Tokenstream[index].u.floatValue;
                    //std::string << (ss.str());
                    //std::cout << "Token type float " << std::fixed << std::setprecision(10)<< ss.str();
                    std::cout << "Token type float " << Tokenstream[index].Data.u.floatValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case DOUBLETYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token type double " << Tokenstream[index].Data.u.doubleValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case LONGDOUBLETYPE:
#if defined (CPP_COMPILER)
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token type LongDouble " << Tokenstream[index].Data.u.longdoubleValue << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
#endif
                    break;
                case NAME:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token stringname  " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                default:
                    break;
            }

            index++;
        }

    } else
    {
        ErrorTracer::PrintErrors();
        ;
    }


}

/*
Component Function: void TestFramework::EvaluateHeaderStack(void)
Arguments: None
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void TestFramework::EvaluateHeaderStack(void)
{


    /* Function being designed */

}

/*
Component Function: void void TestFramework::EvaluateIntermediateStack(void)
Arguments: void
Returns : None
Description:
This function evaluates all headers in intermediate stack.

Nested headers detection and processing Algorithm
-------------------------------------------------
1. List all headers into a std::vector of std::string after reading initial source or header file. All headers in this std::vector are at root 
level.The lexer and parser would  generate this list for each header or source file read.Everytime a header is read, it is first processed by 
comments processing module which detects comments and removes them. Lexer and parser are run on it.
2. Read each header in the above std::vector and run lexer and parser on it and detect if that header has nested headers .If yes, push that header 
into header stack as element of type HeaderData, gather names of nested headers in a std::vector of std::string which is called file Stack. 
Reverse this list and pop each of the header name and push into intermediateHeadersStack as element of type Headerdata. Mark indentation level of 
each header which is put into  intermediateHeaderStack. The order of pushing headers into intermediate stack should be the same as the order of
 nested headers of the above header. If the header doesn't have nested headers then push that header into headerstack.
3. Detect if the header on top of the intermediate header stack has nested headers, if yes, pop that header and push it into headerstack. 
Gather names of nested headers with lexer and parser again in a std::vector of std::string which is called File Stack. Reverse this list and 
pop each of the header name into intermediateHeadersStack as element of type Headerdata . Repeat this step until there is no header with nested 
headers on the top of intermediate headers stack.  If all nested headers in the intermediate stack dont have nested headers in them, then pop
{
 all of them and push them into HeaderStack. Mark indetation of each header as two, three and so on everytime it is inserted into intermediatestack. The order of pushing headers into intermediate stack should be the same as the order of headers read in root level and nested headers read at subsequent levels.
4. Repeat step 3 if intermediate headers stack is not empty.
5. If there are headers at root level which are not processed, then repeat steps 2 to 4 until all headers in the root level are pushed into 
headerstack.
6. Reverse HeaderStack to get correct order of headers and nested headers as in source or header file.

To do:
Automate the above to read files automatically and process all stacks created and used.

Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void TestFramework::EvaluateIntermediateStack(void)
{ /* Function being designed */

}

/*
Component Function: void TestFramework::ClearHeaderData(HeaderData &CurrentHeader)
Arguments: Current header
Returns : None
Description:
Clears Header data.
Version and bug history:
0.1 Initial version.
 */
void TestFramework::ClearHeaderData(HeaderData &CurrentHeader)
{
    CurrentHeader.HeaderContent.clear();
    CurrentHeader.HeaderName.clear();
    CurrentHeader.isHeaderProcessed = false;
    CurrentHeader.IndentationLevel = 300;
    CurrentHeader.NumberOfCommentLines = 0;
    CurrentHeader.NumberOfNonCommentLines = 0;
}

/*
Component Function: void TestFramework::InitParser(void)
Arguments: None
Returns : None
Description:
Initializes parser.Gathers list of header names.

Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void TestFramework::InitParser(void)
{
    TestFramework::ParseTokenstream(TokenList);
    system("pause");
}

/*
Component Function: void TestFramework::ParseTokenstream(std::vector <Token_tag> &TokenStream)
Arguments: Token stream from lexer output
Returns: None
Version : 0.1 Initial version.
Description:
The function would take vector of tokens as a argument and push header names into file stacks.

Notes:
To do:
Detect if a header name is on same line as #include token or same statement as a #include 
Avoid detecting strings as Token_tag in lines as void ParseTokenstream(std::vector <Token_tag> &TokenStream);

 */
void TestFramework::ParseTokenstream(std::vector <Token_tag> &TokenStream)
{
    /*
Measure function execution time.
auto start = std::chrono::steady_clock::now();
std::cout << "in ParseTokenstream() function " << std::endl;
     */
    SINT32 index = ZERO;
    const SINT32 size = TokenStream.size();
    Token_tag CurrentToken;
    Lexer::ClearToken(CurrentToken);
    Token_tag Temp;


    Lexer::ClearToken(Temp);

    SINT32 Count = ZERO;
    Token_tag NextToken;
    Token_tag PreviousToken;
    BOOL FoundNextToken = false;
    BOOL FoundPreviousToken = false;
    BOOL FoundName = false;

    /* Each token in the implementation would be an element of a vector of tokens, TokenList. */
    while (index < size)
    {
        /*
        Look ahead to detect next token which is not whitespace. Look previously to detect a non-whitespace token.
         */
        CurrentToken = TokenStream[index];

        if (CurrentToken.TokenDataType != WHITESPACE)
        {
            Count = index;
            FoundNextToken = false;
            while (Count < size && FoundNextToken == false)
            {
                if ((Count + 1) && (TokenStream[Count + 1].TokenDataType != WHITESPACE))
                {
                    /* std::cout << "Found a token which is not ' ' " << std::endl; */
                    Lexer::ClearToken(NextToken);
                    NextToken = TokenStream[Count + 1];
                    FoundNextToken = true;
                } else
                {
                    Lexer::ClearToken(NextToken);
                }
                Count++;
            }

            /* 
                        Look before to detect previous token which is not whitespace as in a +b;
             */
            Count = index;
            FoundPreviousToken = false;
            while (Count > 0 && FoundPreviousToken == false)
            {
                if ((Count - 1) && (TokenStream[Count - 1].TokenDataType != WHITESPACE))
                {
                    Lexer::ClearToken(PreviousToken);
                    PreviousToken = TokenStream[Count - 1];
                    FoundPreviousToken = true;
                } else
                {
                    Lexer::ClearToken(PreviousToken);
                }
                Count--;
            }

        }



        /*
         Write code to detect a header files in a source or header file such as
         #include "io.h"
         #include <lexer.h>
         
          Detect strings for the above lines as:
          include
          io.h
          include
          lexer.h

        The parser would detect tokens containing strings io.h and lexer.h as Headers and any tokens such as < > or " around header names 
        as begin header, endHeader.

         */
        switch (CurrentToken.TokenDataType)
        {
            case HASHTYPE:
                break;
            case DEFINE:
                break;
            case INCLUDE:
                /* Detect header files here */
                if (OriginalFileProcessed)
                {
                    if ((index >= 1) && (((PreviousToken.TokenDataType == HASHTYPE)) && \
					 ((NextToken.TokenDataType == SYMBOLLESSTHAN) || (NextToken.TokenDataType == QUOTETYPE))))
                    {

                        /* detect if the token following a < or " is a name " */
                        Count = index + 1;
                        FoundName = false;
                        while (Count < size && TokenStream[Count].TokenDataType != NEWLINETYPE && FoundName == false)
                        {
                            if ((Count + 1) && (TokenStream[Count + 1].TokenDataType == NAME))
                            {
                                FoundName = true;
                            }
                            else
                            {
                                Lexer::ClearToken(NextToken);
                            }
                            Count++;
                        }

                        /* Found a header name */
                        CurrentToken = TokenStream[Count];

                        /* Check if next token to a name is > or " */
                        FoundNextToken = false;
                        while (Count < size && TokenStream[Count].TokenDataType != NEWLINETYPE && FoundNextToken == false)
                        {
                            if ((Count + 1) && ((TokenStream[Count + 1].TokenDataType == SYMBOLGREATERTHAN) || (TokenStream[Count + 1].TokenDataType == QUOTETYPE)))
                            {
                                FoundNextToken = true;
                            }
                            else
                            {
                                Lexer::ClearToken(NextToken);
                            }
                            Count++;
                        }


                        if ((FoundName) && (FoundNextToken))
                        {
                            std::cout << "Found a header name " << CurrentToken.Data.u.cString << std::endl;
                            Stack::PushOriginalSourceStack(CurrentToken.Data.u.cString);
                            FoundName = false;
                            FoundNextToken = false;
                        }

                    }
                    /* Push header string name into list of header files in original source */
                } else
                {
                    /* Push header string name into list of header files in original source */
                    if ((index >= 1) && (((PreviousToken.TokenDataType == HASHTYPE)) && \
					 ((NextToken.TokenDataType == SYMBOLLESSTHAN) || (NextToken.TokenDataType == QUOTETYPE))))
                    {

                        /* detect if the token following a < or " is a name " */
                        Count = index + 1;
                        FoundName = false;
                        while (Count < size && TokenStream[Count].TokenDataType != NEWLINETYPE && FoundName == false)
                        {
                            if ((Count + 1) && (TokenStream[Count + 1].TokenDataType == NAME))
                            {
                                FoundName = true;
                            }
                            else
                            {
                                Lexer::ClearToken(NextToken);
                            }
                            Count++;
                        }

                        /* Found a header name */
                        CurrentToken = TokenStream[Count];

                        /* Check if next token to a name is > or " */
                        FoundNextToken = false;
                        while (Count < size && TokenStream[Count].TokenDataType != NEWLINETYPE && FoundNextToken == false)
                        {
                            if ((Count + 1) && ((TokenStream[Count + 1].TokenDataType == SYMBOLGREATERTHAN) || (TokenStream[Count + 1].TokenDataType == QUOTETYPE)))
                            {
                                FoundNextToken = true;
                            }
                            else
                            {
                                Lexer::ClearToken(NextToken);
                            }
                            Count++;
                        }


                        if ((FoundName) && (FoundNextToken))
                        {
                            std::cout << "Found a header name " << CurrentToken.Data.u.cString << std::endl;
                            Stack::PushFileStack(CurrentToken.Data.u.cString);
                            FoundName = false;
                            FoundNextToken = false;
                        }

                    }
                }
                break;
            case NAME:

                break;
            case ARGSEPARATOR:
                break;
            case RIGHTPARENTHESIS:
                break;
            case LEFTPARENTHESIS:
                break;
            case NEWLINETYPE:
                break;

            case WHITESPACE:
                break;

            default:
                break;
        }
        index++;

    }


    //Measure execution time.
    //auto end = std::chrono::steady_clock::now();
    //auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    ////std::cout << "Execution time " << diff << " nanoseconds" << std::endl;

    //std::cout << "End of ParseTokenstream() function " << std::endl;
}

/*
Component Function: void TestFramework::DetectHeaders(void)
Arguments: None
Returns : None
Description:
This function searches all headers and lists them in a stack with a non recursive approach.
Version and bug history:
0.01 Initial version.
0.02 Added some lines for intermediatestack
Notes: Function being designed. Code being developed.

Notes:

To do:
Detect if multiple headers have same identation level such as mutiple headers having indentation level of +1, +2 or so on.
If multiple headers such as header1, header2 and header3 with same indetation are detected:
Detect if header1 has nested headers , if yes, assign IndentationLevelofParent = Indentation of (header1);
Gather its nested headers and detect if they have nested headers. iF not increment their indentation level until reaching the last 
nested header. Pop all of them and push them into header stack. if its nested headers have nested headers then push them into 
intermediate stack.
Repeat the above for header2 and header3.

Some methods:
Create a data structure for indentation of each header and its nested headers. unordered map or trie? 

Attach nested headers to their parents by linkage.
Each Child header would have +1 indentation level compared to its parent.

*/
void TestFramework::DetectHeaders(void)
{
    std::string Filename = "main.cpp";
    std::cout << "Input source file is: " << Filename << std::endl;
    OriginalFileProcessed = true;
    HeaderData CurrentHeader;
    std::string Headername;
    UINT32 IndentationLevel = 0;
    UINT32 IndentationLevelofParent=0;
    UINT32 IndentationLevelofChild=0;
    //BOOL HeadersWithSameIndentationDetected=false;
    //UINT32 Temp=0;
    //BOOL isIntermediateStackEmpty =false;
    HeadersProcessing::ClearHeaderData(CurrentHeader);
    CommentsProcessing::InitCommentsProcessing(Filename);
    Lexer::InitLexer();
    Parser::InitParser();
    if (OriginalFileProcessed)
    {
        Stack::ReverseOriginalSourceStack();
        OriginalFileProcessed = false;
        //Stack::PrintOriginalSourceStack();
    }

    while (!Stack::isOriginalSourceStackEmpty())
    {
        //std::cout << "Getting a header name in original source stack "<< std::endl;
        Stack::PopOriginalSourceStack(Headername);
        /* These statements write nested header names of each header into a file stack */
        CommentsProcessing::InitCommentsProcessing(Headername);
        Lexer::InitLexer();
        Parser::InitParser();
        //#if(0)
        Stack::PrintFileStack();
        /* Print headers in intermediate stack */
        Stack::PrintIntermediateStack();
        /* Print headers Stack */
        Stack::PrintHeaderStack();
        //#endif
        
        
        IndentationLevel = 0;
        IndentationLevelofParent=0;

        /* Get header names from file stack and check if each header has nested headers */
        /* Code review begin*/
        if (Stack::isFileStackEmpty())
        {
            /* This header has no nested headers ,push it into Header stack*/
            HeadersProcessing::ClearHeaderData(CurrentHeader);
            //std::cout << Headername << " header does not have nested headers"<< std::endl;
            //TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
            TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevelofParent, true);
            //std::cout << "Pushing current header into header stack"<< std::endl;
            Stack::PushHeaderStack(CurrentHeader);
            //std::cout << "Name of header "<< Headername << std::endl;
        } else if (!Stack::isFileStackEmpty())
        {
            /* Indentation should be changed for each file from here */
            HeadersProcessing::ClearHeaderData(CurrentHeader);
            //std::cout << Headername << " header has nested headers"<< std::endl;
            //SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
            SetHeaderData(CurrentHeader, Headername, IndentationLevelofParent, true);
            //std::cout << "Pushing current header into header stack"<< std::endl;
            Stack::PushHeaderStack(CurrentHeader); // Changed line 
            //Stack::PushHeaderIntermediateStack(CurrentHeader);
            //system("pause");
            // Reversing file stack necessary
            Stack::ReverseFileStack();
            //#if(0)
            //std::cout << "Priting list of nested headers"<< std::endl;
            Stack::PrintFileStack();
            /* Print headers in intermediate stack */
            Stack::PrintIntermediateStack();
            /* Print headers Stack */
            Stack::PrintHeaderStack();
            //#endif
            /* Try setting indentation level only once for headers inserted into intermediate stack. Any nested headers of 
    		a header which is put into header stack and its nested headers put in intermediate stack should not increase their indentation level.
			These nested nested headers when put onto intermediate stack are increasing their offset a lot more than their original offset should be.*/
            IndentationLevel++;
            /* Get names of headers in file stack and check if they are having nested headers or not */
            IndentationLevelofChild=IndentationLevelofParent+1;
            
            if (!Stack::isFileStackEmpty())
            {
              	// All elements in file stack would have same indentation level.This condition would be used to set indenation of all headers detected.
              	//within a header and inserted into intermediateHeaderStack.
              	// Elements within file stack would have indentation as IdentofChild
     		    //IndentofChild=IndentofParent+1;	
     		    //HeadersWithSameIndentationDetected=true;
			}
			
            while (!Stack::isFileStackEmpty())
            {
            	/* These are header names at first level as in 
				#include "map.h"  and map.h includes "File.h" , so keep indenation of each subheader as +1 */

                Stack::PopFileStack(Headername);
                CommentsProcessing::InitCommentsProcessing(Headername);
                HeadersProcessing::ClearHeaderData(CurrentHeader);
                //TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
                TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevelofChild, true);
                std::cout << "Pushing current header into intermediate header stack" << std::endl;
                TestFramework::PrintCurrentHeader(CurrentHeader);
                Stack::PushHeaderIntermediateStack(CurrentHeader);
                //Temp=IndentationLevel;
                std::cout << "Name of header "<< Headername << " Indentation " << IndentationLevelofChild<< std::endl;
                //system("pause");
            }
            
            
            
            /* reverse intermediate stack? */
            Stack::ReverseIntermediateHeaderStack();
            /* Print File stack */
            //#if(0)
            Stack::PrintFileStack();
            /* Print headers in intermediate stack */
            Stack::PrintIntermediateStack();
            /* Print headers Stack */
            Stack::PrintHeaderStack();
            //#endif

            //system("pause");
            // Verified code till here.
            /* Write features to increase indentation level of parent if there are more nested headers to process within each nested header. */
            while (!Stack::isIntermediateHeaderStackEmpty())
            {
                //std::cout << "\nInside Intermediate header stack" << std::endl;
                HeadersProcessing::ClearHeaderData(CurrentHeader);
                /* pop Current header and then push it into header stack if doesn't contain nested headers, push all nested headers
                if any into intermediate stack again.*/
                std::cout << "Getting the topmost header in Intermediate Stack" << std::endl;
                Stack::PopHeaderIntermediateStack(CurrentHeader);
                TestFramework::PrintCurrentHeader(CurrentHeader);
                Headername = CurrentHeader.HeaderName;
                CommentsProcessing::InitCommentsProcessing(Headername);
                Lexer::InitLexer();
                Parser::InitParser();
                IndentationLevelofParent=CurrentHeader.IndentationLevel; 
                //Stack::ReverseFileStack();
                //#if(0)
                Stack::PrintFileStack();
                /* Print headers in intermediate stack */
                Stack::PrintIntermediateStack();
                /* Print headers Stack */
                Stack::PrintHeaderStack();
                //#endif
                /* Check if file stack is empty or not , if file stack is 
                empty push header into header stack or else push header into intermediate stack.
                 */
                 
                 /* Change this block , include detection of multiple headers with nested headers which have same indentation level. Update their
				 Indentation Level to their parents.*/
                  /*
                   Change of a intermediate header which has children and is popped out of intermediateHeaderStack should be detected and the indentation level
				   of it as parent node should be reassigned. Nested headers of nested headers are not assigned indent levels correctly. 
				   Here is the line which should assign correct indent level of each parent which is the header which is popped and has nested headers. 
				   An example is Memory.h in this program. Do this for each header that has nested headers and is processed as above.
                  */
                 //system("pause");
                    
                 if (!Stack::isFileStackEmpty())
                 {
                	// All elements in file stack would have same indentation level.This condition would be used to set indenation of all headers detected.
                	//within a header and inserted into intermediateHeaderStack.
                	// Elements within file stack would have indentation as IdentofChild
     				//IndentofChild=IndentofParent+1;	
     				//HeadersWithSameIndentationDetected=true;
			     }
                 if (!Stack::isFileStackEmpty())
                 {
                 	/* A header with nested headers has been detected. Now, we will push this header into header stack and push back elements of
                 	File stack into Intermediate stack with IndentationLevel */
                 	IndentationLevelofChild=IndentationLevelofParent+1;
                 	std::cout << "Current Header and Current indentation level at CurrentHeader.IndentationLevel" << std::endl;
                 	TestFramework::PrintCurrentHeader(CurrentHeader); 
                 	//HeadersWithSameIndentationDetected=true;
				 }
				 
                if (!Stack::isFileStackEmpty())
                {
                    //std::cout << Headername << " header has nested headers"<< std::endl;
                    /* Push topmost header of intermediate stack into header stack after gathering its
                    nested headers .if the topmost header doesnt have nested headers, push it into header stack.
                    Change indentLevel.
                     */
    
                    Stack::PushHeaderStack(CurrentHeader);
                    /* Try setting indentation level only once for headers inserted into intermediate stack. Any nested headers of 
					a header which is put into header stack and its nested headers put in intermediate stack should not increase their indentation level.
					These nested nested headers when put onto intermediate stack are increasing their offset a lot more than their original offset should be.*/
					IndentationLevel++;
             
                    while (!Stack::isFileStackEmpty())
                    {
                        Stack::PopFileStack(Headername);
                        /* Code edit here */
                        CommentsProcessing::InitCommentsProcessing(Headername);
                        HeadersProcessing::ClearHeaderData(CurrentHeader);
                        /* Changed line */
                        //TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
						TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevelofChild, true);
                        //std::cout << "Pushing current header into intermediate header stack"<< std::endl;
                        TestFramework::PrintCurrentHeader(CurrentHeader);
                        Stack::PushHeaderIntermediateStack(CurrentHeader);
                    }

                    
                } else if (Stack::isFileStackEmpty())
                {
                	
                    std::cout << Headername << " Header does not have nested headers"<< std::endl;
                    CommentsProcessing::InitCommentsProcessing(Headername);
                    HeadersProcessing::ClearHeaderData(CurrentHeader);
                    //TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
					/* This header has no nested headers , push it directly into header stack */
                    TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevelofParent, true);
                    TestFramework::PrintCurrentHeader(CurrentHeader);
                    //std::cout << "Pushing current header into header stack"<< std::endl;
                    Stack::PushHeaderStack(CurrentHeader);
                }
                
                /* Change this block */
                
            }

            //std::cout << "\nFinished processing Intermediate header stack" << std::endl;
            
            /* Clear file stack */
            Stack::ClearFileStack();
        }
        /* Code review end */
        IndentationLevel = 0;
    }
    /* Reverse header stack */
    Stack::ReverseHeaderStack();
    
    TestFramework::CorrectIndentationLevelsinHeaderStack(HeaderStack);
    TestFramework::DisplayHeaderStack(HeaderStack);
}




/*
Component Function: BOOL TestFramework::DetectSameIndentationofHeaders(void)
Arguments: Header container and Header name name
Returns : None
Description:
This function uses intermediateHeaderstack and determines if it contains headers with same indentation, if yes, then 
a true is returned. If all headers have unique indentation, then a false is returned.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
BOOL TestFramework::DetectSameIndentationofHeaders(void)
{
	BOOL HeaderswithSameIndentationDetected=false;
	
	
	return HeaderswithSameIndentationDetected;
}
 
/*
Component Function: void  TestFramework::SetHeaderData(HeaderData& CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
Arguments: Header container and Header name name
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void TestFramework::SetHeaderData(HeaderData& CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
{
    HeadersProcessing::ClearHeaderData(CurrentHeader);
    CurrentHeader.HeaderName = Filename;
    CurrentHeader.HeaderContent = s;
    CurrentHeader.IndentationLevel = IndentationLevel;
    CurrentHeader.isHeaderProcessed = isHeaderProcessed;
    CurrentHeader.NumberOfCommentLines = CurrentFile.NumberofCommentLines;
    CurrentHeader.NumberOfNonCommentLines = CurrentFile.NumberofNonCommentLines;
    CurrentHeader.NumberofTotalLines = CurrentFile.NumberofLines;
}

/*
Component Function: void  void TestFramework::PrintCurrentHeader(HeaderData& CurrentHeade)
Arguments: Header container and Header name name
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void TestFramework::PrintCurrentHeader(HeaderData& CurrentHeader)
{
    std::cout << "Header name in Current Header is: " << CurrentHeader.HeaderName << " Indentation " << CurrentHeader.IndentationLevel << std::endl;

}


/*
Component Function: void TestFramework::CorrectIndentationLevelsinHeaderStack(const std::vector <HeaderData> &pS)
Arguments:  Reference to stack.
Returns: None
Description:  Adjusts indentation levels in header stack.
Version : 0.1
Notes:
 */
void TestFramework::CorrectIndentationLevelsinHeaderStack(const std::vector <HeaderData> &pS)
{
	SINT32 size =pS.size()-1;
	SINT32 index=0;
	HeaderData CurrentHeader;
	HeaderData Temp;
	//UINT32 NestedHeaderPosition=0;
	
	HeadersProcessing::ClearHeaderData(CurrentHeader);
	HeadersProcessing::ClearHeaderData(Temp);
	//UINT32 identLevel =0;
	for (index=size;index>=0;index--)
	{
		if (pS[index].IndentationLevel ==1)
		{
			//NestedHeaderPosition=index;
			
		}
		
		
	}
	
}

/*
Component Function: void Stack::DisplayHeaderStack(const std::vector <HeaderData> &pS)
Arguments:  Reference to stack.
Returns: None
Description:  Displays Header stack
Version : 0.1
Notes:
 */
void TestFramework::DisplayHeaderStack(const std::vector <HeaderData> &pS)
{
    std::cout << "Source tree with indentation of headers is:" << std::endl;
    SINT32 index = 0;
    UINT32 Count=0;
    if (!(Stack::isHeaderStackEmpty()))
    {
        for (index = pS.size() - 1; index >= 0; index--)
        {
        	//#if(0)
        	
        	for(Count=0;Count<(pS[index].IndentationLevel)*4;Count++)
        	{
        		std::cout<< " ";
			}
			//#endif
			
            std::cout << pS[index].HeaderName <<  " - (" << pS[index].IndentationLevel << ")" <<  std::endl;
            // Use this code while filling Postfix and operator stack.
        }
    } else
    {
        std::cout << "Header stack empty" << std::endl;

    }
}



#if(0)
/* Copy */


/*
Component Function: void TestFramework::DetectHeaders(void)
Arguments: None
Returns : None
Description:
This function searches all headers and lists them in a stack with a non recursive approach.
Version and bug history:
0.01 Initial version.
0.02 Added some lines for intermediatestack
Notes: Function being designed. Code being developed.

Notes:
Indentation being designed.

To do:
Indentation of different headers while pushing them into intermediate stack and header stack and using file stack many times.
Indentation should be different or unique for nested headers of different headers depending on number of nested headers of each header.
map.h, bits.h Memory.h, std_types.h and File.h are only added into headerstack and utility.h,memory.h are skipped. 
Make indentation unique for each header and its nested headers. 

Some methods:
Create a data structure for indentation of each header and its nested headers. unordered map or trie? 

Attach nested headers to their parents by linkage.
Separate each header included by a marker.
Iterate through header stack and assign indetation levels by detecting parent-child headers and markers.
Each Child header would have +1 indentation level compared to its parent.

Problem:
Indetation of first level nested headers is incorrect if they don't have nested headers. 


if header 1 and header 2 are nested headers of header0
as in:

header1
header2

and header 1 has header 3, header4 as nested headers.

then header 2 is getting the same indentation of last nested header of header 1.
Every intermediate header other than first has wrong indentation of its nested headers. 
Correct indentation.

*/
void TestFramework::DetectHeaders(void)
{
    std::string Filename = "main.cpp";
    std::cout << "Input source file is: " << Filename << std::endl;
    OriginalFileProcessed = true;
    HeaderData CurrentHeader;
    std::string Headername;
    UINT32 IndentationLevel = 0;
    UINT32 IndentLevelofParent=0;
    UINT32 IndentLevelofChild=0;
    //BOOL isIntermediateStackEmpty =false;
    HeadersProcessing::ClearHeaderData(CurrentHeader);
    CommentsProcessing::InitCommentsProcessing(Filename);
    Lexer::InitLexer();
    Parser::InitParser();
    if (OriginalFileProcessed)
    {
        Stack::ReverseOriginalSourceStack();
        OriginalFileProcessed = false;
        //Stack::PrintOriginalSourceStack();
    }

    while (!Stack::isOriginalSourceStackEmpty())
    {
        //std::cout << "Getting a header name in original source stack "<< std::endl;
        Stack::PopOriginalSourceStack(Headername);
        /* These statements write nested header names of each header into a file stack */
        CommentsProcessing::InitCommentsProcessing(Headername);
        Lexer::InitLexer();
        Parser::InitParser();
        //#if(0)
        Stack::PrintFileStack();
        /* Print headers in intermediate stack */
        Stack::PrintIntermediateStack();
        /* Print headers Stack */
        Stack::PrintHeaderStack();
        //#endif

        /* Get header names from file stack and check if each header has nested headers */
        /* Code review begin*/
        if (Stack::isFileStackEmpty())
        {
            IndentationLevel = 0;
            HeadersProcessing::ClearHeaderData(CurrentHeader);
            //std::cout << Headername << " header does not have nested headers"<< std::endl;
            TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
            //std::cout << "Pushing current header into header stack"<< std::endl;
            Stack::PushHeaderStack(CurrentHeader);
            //std::cout << "Name of header "<< Headername << std::endl;
        } else if (!Stack::isFileStackEmpty())
        {
            /* Indentation should be changed for each file from here */
            HeadersProcessing::ClearHeaderData(CurrentHeader);
            //std::cout << Headername << " header has nested headers"<< std::endl;
            SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
            //std::cout << "Pushing current header into header stack"<< std::endl;
            Stack::PushHeaderStack(CurrentHeader); // Changed line 
            //Stack::PushHeaderIntermediateStack(CurrentHeader);
            //system("pause");
            // Reversing file stack necessary?
            Stack::ReverseFileStack();
            //#if(0)
            //std::cout << "Priting list of nested headers"<< std::endl;
            Stack::PrintFileStack();
            /* Print headers in intermediate stack */
            Stack::PrintIntermediateStack();
            /* Print headers Stack */
            Stack::PrintHeaderStack();
            //#endif
            /* Try setting indentation level only once for headers inserted into intermediate stack. Any nested headers of 
    		a header which is put into header stack and its nested headers put in intermediate stack should not increase their indentation level.
			These nested nested headers when put onto intermediate stack are increasing their offset a lot more than their original offset should be.
			Detect indentation of headers by detecting the indentation of its parent header.Keep track of every header and its initial indentation level
			subsequent headers should have their indentation levels set by following a chain.*/
            IndentationLevel++;
            IndentLevelofParent=IdentationLevel;
            IndentofChild=IndentofParent+1;	
            
            if (!Stack::isFileStackEmpty())
            {
            	// All elements in file stack would have same indentation level.This condition would be used to set indenation of all headers detected
            	//within a header and inserted into intermediateHeaderStack.
            	// Elements within file stack would have indentation as IdentofChild
				//IndentofChild=IndentofParent+1;	
			}
            /* Get names of headers in file stack and check if they are having nested headers or not */
            while (!Stack::isFileStackEmpty())
            {

                Stack::PopFileStack(Headername);
                CommentsProcessing::InitCommentsProcessing(Headername);
                HeadersProcessing::ClearHeaderData(CurrentHeader);
                
                
                //TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
                TestFramework::SetHeaderData(CurrentHeader, Headername, IndentofChild, true);
                std::cout << "Pushing current header into intermediate header stack" << std::endl;
                Stack::PushHeaderIntermediateStack(CurrentHeader);
                //std::cout << "Name of header "<< Headername << " Indentation " << IndentationLevel<< std::endl;
                //system("pause");
                
            }

            /* reverse intermediate stack? */
            Stack::ReverseIntermediateHeaderStack();
            /* Print File stack */
            //#if(0)
            Stack::PrintFileStack();
            /* Print headers in intermediate stack */
            Stack::PrintIntermediateStack();
            /* Print headers Stack */
            Stack::PrintHeaderStack();
            //#endif
            while (!Stack::isIntermediateHeaderStackEmpty())
            {
                //std::cout << "\nInside Intermediate header stack" << std::endl;
                HeadersProcessing::ClearHeaderData(CurrentHeader);
                /* pop Current header and then push it into header stack if doesn't contain nested headers, push all nested headers
                                if any into intermediate stack again.*/
                //std::cout << "Getting the topmost header in Intermediate Stack" << std::endl;
                Stack::PopHeaderIntermediateStack(CurrentHeader);
                TestFramework::PrintCurrentHeader(CurrentHeader);
                Headername = CurrentHeader.HeaderName;
                CommentsProcessing::InitCommentsProcessing(Headername);
                Lexer::InitLexer();
                Parser::InitParser();
                //Stack::ReverseFileStack();
                //#if(0)
                Stack::PrintFileStack();
                /* Print headers in intermediate stack */
                Stack::PrintIntermediateStack();
                /* Print headers Stack */
                Stack::PrintHeaderStack();
                //#endif
                
                IndentLevelofParent=IdentationLevel;
                /* Check if file stack is empty or not , if file stack is 
                                empty push header into header stack or else push header into intermediate stack.
                 */
                 
                if (!Stack::isFileStackEmpty())
                {
            	//This condition would be used to set indenation of all headers detected
            	//within a header and inserted into intermediateHeaderStack.All elements in file stack would have same indentation level.
            	// Elements within file stack would have indentation as IdentofChild
				//IndentofChild=IndentofParent+1;	
				
			    } 
			    
                if (!Stack::isFileStackEmpty())
                {
                    //std::cout << Headername << " header has nested headers"<< std::endl;
                    /* Push topmost header of intermediate stack into header stack after gathering its
                    nested headers .if the topmost header doesnt have nested headers, push it into header stack.
                     */
                    Stack::PushHeaderStack(CurrentHeader);
                    /* Set indentation level only once for headers inserted into intermediate stack. Any header which is put into header stack and its 
					nested headers put in intermediate stack should not increase their indentation level beyond the nested headers of a previous nested header in intermediate stack 
					which underwent a similar process.
					These nested nested headers when put onto intermediate stack are increasing their offset a lot more than their original offset should be.
					Detect indentation of headers by detecting the indentation of its parent header.Keep track of every header and its initial indentation level
			        subsequent headers should have their indentation levels set by following a chain.*/
			        /* Change indentation algorithm as above */
					IndentationLevel++;
                    IndentofChild=IndentofParent+1;	
                    while (!Stack::isFileStackEmpty())
                    {
                        Stack::PopFileStack(Headername);
                        /* Code edit here */
                        CommentsProcessing::InitCommentsProcessing(Headername);
                        HeadersProcessing::ClearHeaderData(CurrentHeader);
						TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
                        //TestFramework::SetHeaderData(CurrentHeader, Headername, IndentofChild, true);
                        //std::cout << "Pushing current header into intermediate header stack"<< std::endl;
                        TestFramework::PrintCurrentHeader(CurrentHeader);
                        Stack::PushHeaderIntermediateStack(CurrentHeader);
                        /* Code change here */
                        /* Code edit here */
                        IndentofChild++;
                        
                    }
                } else if (Stack::isFileStackEmpty())
                {
                    std::cout << Headername << " header does not have nested headers"<< std::endl;
                    CommentsProcessing::InitCommentsProcessing(Headername);
                    HeadersProcessing::ClearHeaderData(CurrentHeader);
                    TestFramework::SetHeaderData(CurrentHeader, Headername, IndentationLevel, true);
                    TestFramework::PrintCurrentHeader(CurrentHeader);
                    //std::cout << "Pushing current header into header stack"<< std::endl;
                    Stack::PushHeaderStack(CurrentHeader);
                }
            }

            //std::cout << "\nFinished processing Intermediate header stack" << std::endl;
        }
        /* Code review end */
        IndentationLevel = 0;
    }
    /* Reverse header stack */
    Stack::ReverseHeaderStack();
    
    TestFramework::CorrectIndentationLevelsinHeaderStack(HeaderStack);
    TestFramework::DisplayHeaderStack(HeaderStack);
}

#endif