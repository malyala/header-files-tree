/*
Name :  TextAnalysis.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2017-Current. All rights reserved.
Date : 20-06-2016
Description:
This module contains a word parser function which will extract all words or strings separated
by ' '
 
Notes:

Bug and revision history;
0.1 Initial version
 */

#include "TextAnalysis.h"
#include <iostream>
#include "Stack.h"

/* Define a vector for storing words in a heade */
std::vector <std::string> WordList;

/*
Component Function: void TextAnalysis::TextAnalysis(const std::vector <UINT8>& str)
Arguments: String which is to be parsed, vector which stores all substrings in the string.
returns : None
Description
Function to detect words or substrings separated by ' '.
Notes:
Function being designed.

Version and bug history : 
0.1 Initial version.
 */
void TextAnalysis::TextAnalysis(const std::vector <UINT8>& str)
{

    // Measure function execution time.
    //auto start = std::chrono::steady_clock::now();
    std::string substring;
    UINT32 i = 0;

    // Clear and initialize Word list 
    WordList.clear();
    
    //std::cout  << "In text analysis function " << std::endl;
    

    UINT32 StrLength = str.size();
    while (i <= StrLength)
    {
        /*
        Use a switch statement here to detect alphabetical and !@#$%^?&*()_|\~`",'.=+-[]{};:/ characters or 
            numbers 0123456789 . A word can contain any of above if its separated by other words by a ' ' or '\n'.
        */
        switch (str[i])
        {
            // Reading alphabetical chars	
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
            case 'g':
            case 'h':
            case 'i':
            case 'j':
            case 'k':
            case 'l':
            case 'm':
            case 'n':
            case 'o':
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case 't':
            case 'u':
            case 'v':
            case 'w':
            case 'x':
            case 'y':
            case 'z':
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
            case 'G':
            case 'H':
            case 'I':
            case 'J':
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case 'O':
            case 'P':
            case 'Q':
            case 'R':
            case 'S':
            case 'T':
            case 'U':
            case 'V':
            case 'W':
            case 'X':
            case 'Y':
            case 'Z':
                //std::cout <<"Reading a Alphabetical char"<< std::endl;
                // reading special chars
                //case '!':
            case '@':
            case '#':
            case '$':
            case '%':
            case '^':
                //case '?':
            case '&':
            case '*':
            case '(':
            case ')':
            case '_':
            case '|':
            case '\\':
            case '~':
            case '`':
            case '"':
            case '>':
            case '<':
            case '\'':
            case '=':
            case '+':
            case '[':
            case ']':
            case '{':
            case '}':
            case ';':
                //case ':':
            case '/':
            

                //std::cout <<"Reading a Special char"<< std::endl;

                // Now reading digits
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                //std::cout <<"Reading a decimal"<< std::endl;


                // Reading a space before or after a word
   
                // Reading a , . : ? - ! before or after a word.
            case ',':
            case '.':
            case '?':
            case '!':
            case ':':
            case '-':
                //std::cout <<"Reading a ? , . : - "<< std::endl;

            case '\0':
                //std::cout <<"Reading end of string \0"<< std::endl;
                // Add each extracted word to a vector
                substring+=str[i];
                i++;
                break;

            case ' ':
            case '\n':	
                //std::cout <<"Reading a ' ' or \n"<< std::endl;
                if (substring != "")
                {
                    // Add each extracted word to a vector
                    WordList.push_back(substring);
                    //std::cout << "Substring is " << substring << std::endl;

                }
                substring = "";
                i++;
                break;    

                // Reading other characters which are possibly extended ascii chars.	
            default:
                //std::cout <<"Reading a extended ascii char"<< std::endl;
                i++;
                break;
        }


    } // End of while
    // Text parsing code ends

    // Measure execution time.
    //auto end = std::chrono::steady_clock::now();
    //auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    //std::cout << "Execution time " << diff << " nanoseconds";
    
    //std::cout << "WordList Size is " << WordList.size() << std::endl;

}


/*
Component Function: void TextAnalysis::PrintContentofHeaders(const std::vector <std::string> &WordList)
Arguments: Stack of headers.
returns : None
Description
Print words in a header
Notes:
Function being designed.
Version and bug history : 
0.1 Initial version.
 */
void TextAnalysis::PrintContentofHeaders(const std::vector <std::string> &WordList)
{
	UINT32 index=0;
	UINT32 size =WordList.size();
	for(index=0;index<size;index++)
	{
		std::cout << WordList[index] << std::endl;
	}
}

/*
Component Function: void TextAnalysis::PrintHeaders(void);
Arguments: None
returns : None
Description
Print c headers and nested headers in header stack.
Notes:
Function being designed.
Version and bug history : 
0.1 Initial version.
 */
void TextAnalysis::PrintHeaders(void)
{
	HeaderData CurrentHeader;
	if (!Stack::isHeaderStackEmpty())
	{
		while (!Stack::isHeaderStackEmpty())
		{
			HeadersProcessing::ClearHeaderData(CurrentHeader);
			Stack::PopHeaderStack(CurrentHeader);
			TextAnalysis::TextAnalysis(CurrentHeader.HeaderContent);
			std::cout << "Header name: " << CurrentHeader.HeaderName << std::endl;
            std::cout << "Number of comment lines " << CurrentHeader.NumberOfCommentLines << std::endl;
            std::cout << "Number of non comment lines " << CurrentHeader.NumberOfNonCommentLines << std::endl;
            std::cout << "Number of total lines " << CurrentHeader.NumberofTotalLines << std::endl;
			TextAnalysis::PrintContentofHeaders(WordList);
	    }
	}
	else
	{
		std::cout << "There are no header files included. Execute option 1 first. " << std::endl;
	}
		
}
