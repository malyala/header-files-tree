/*
Name: Lexer.cpp
Copyright: Amit Malyala, 2016-Current. All rights reserved.
Author: Amit Malyala
Date: 09-06-17 14:21
Description:
This module is a lexical analyzer or scanner and it creates a token list from an input file or string and adds them to a vector.

To do:
Make the module scalable and modifiable to add more features of C++ preprocessing.
Reset column number after each new line. 
Add correct column number to each token.#include "io.h" is assigning column number 2 to '#' token.

Detect string names as Tree.h in     #include "Tree.h" added in the middle of a header or source file.
Lexer should detect tokens of header files in the middle or end of a source or header file.

Notes:
This module would parse header files and store them in a vector of strings for storing headers.
Tokenize all data in source files and read the source file as vector of tokens.
*/

#include "Io.h"
#include "Lexer.h"
#include <string>
#include "DataStructures.h"
#include <iostream>
#include <cstring>
#include <iomanip>
#include <sstream>
#include "Io.h"

#include "Preprocessor.h"
#include "CommentsProcessing.h"

/* Define a vector for storing tokens */
std::vector <Token_tag> TokenList;

/* Define a vector for Parenthesis */
extern std::vector <Token_tag> ParenthesisList;
/* Expression string */
extern std::vector <UINT8> s;

/* is the parser reading original source file */
extern BOOL OriginalFileProcessed;
// Uncomment to test this module.
// Comment to integrate with a project.
#if(0)
SINT32 main(void)
{
   std::string Filename ="io.cpp";
   OriginalFileProcessed=true;
   HeaderData aHeader;
   HeadersProcessing::ClearHeaderData(aHeader);
   // Read also from a file by modifying the lines below.
   //std::cout << "You entered: " << std::endl;
   //io::DisplayOutput(s);
   CommentsProcessing::InitCommentsProcessing(Filename);
   if (ErrorTracer::CheckforErrors() == NO_ERRORS)
   {
   	 Lexer::InitLexer();
     Lexer::DisplayTokenList(TokenList);
   }
   else
   {
   	 ErrorTracer::PrintErrors();
   }
   
   return 0;
}
#endif

/*
Component Function:  void Lexer::InitLexer(void))
Arguments:  Filename
returns: None
Description:
Initialize Lexer module.
Version : 0.1
 */
void Lexer::InitLexer(void)
{
    TokenList.clear();
    ParenthesisList.clear();
    Lexer::GenerateLexerTokens(TokenList);
}

/*
 Component Function: static void Lexer::ClearToken(Token_tag &Token)
 Arguments:  None
 returns: None
 Description:
 Clears a token to read next token
 Version : 0.1
 */
void Lexer::ClearToken(Token_tag &Token)
{
    Token.Meta = "NONE";
    Token.LineNumber = ZERO;
    Token.ColumnNumber = ZERO;
    Token.TokenDataType = NOTADATATYPE;
    Token.Data.u.cString = "NONE";
}


/*
Component Function:  void Lexer::GenerateLexerTokens(std::vector <Token_tag>& TokenStream)
Arguments: Tokenstream vector, Current header
returns: None
Description:
Test case checks design which Generates relevant Lexer tokens from a input string.
Version : 0.2
Notes:

    Processing Headers in source files.
    -------------------------------------
    #include "io.h"
    #include "Lexer.h"

    This function should generate  tokens for source or header files with headers as:

    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER io.h
    Token - ENDHEADER
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER Lexer.h
    Token - ENDHEADER
    Token - NEWLINE

To do:
  Detect strings for the above lines as:
  include
  io.h
  include
  lexer.h
  
  The parser would configure tokens containing strings io.h and lexer.h as Headers and any tokens such as < > or " around header names 
  as beginheader, endHeader.
*/
void Lexer::GenerateLexerTokens(std::vector <Token_tag>& TokenStream)
{
    UINT32  NumberofCharacterssinFile = s.size();
    UINT32 index = ZERO;
    static std::string NumericString;
    static std::string CharString;
    UINT32 ErrorsFound = false;
    Token_tag Token;
    UINT32 LineNumber=ONE;
    UINT32 ColumnNumber=ONE;
    //UINT32 NumericType = NOTADATATYPE;
    /*  NumberofCharacterssinFile variable should be changed to NumberofCharacters in file */
    if ( NumberofCharacterssinFile)
    {
        while (index <  NumberofCharacterssinFile && ErrorsFound == false)
        {
            Lexer::ClearToken(Token);
            switch (s[index])
            {
                case SPACE:
                    Token.Meta = "WHITESPACE";
                    Token.TokenDataType = WHITESPACE;
                    Token.Data.u.cString = " ";
                    Token.ColumnNumber = ColumnNumber;
                    Token.LineNumber = LineNumber;
                    TokenStream.push_back(Token);
                    break;
                case HASH:
                    Token.Meta = "HASH";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = HASHTYPE;
                    Token.Data.u.cString = "#";
                    TokenList.push_back(Token);
                    break;
                case QUOTE:
                    Token.Meta = "QUOTE";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = QUOTETYPE;
                    Token.Data.u.cString = "\"";
                    TokenList.push_back(Token);
                    break;    
                case NEWLINE:
                	Token.Meta = "NEWLINE";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = NEWLINETYPE;
                    Token.Data.u.cString = "\n";
                    TokenList.push_back(Token);
                    LineNumber++;
                    ColumnNumber=1;
                   break;
                case LESSTHAN:
				     Token.Meta="LESSTHAN";
				     Token.LineNumber=LineNumber;
				     Token.ColumnNumber=ColumnNumber;
				     Token.TokenDataType=SYMBOLLESSTHAN;
				     Token.Data.u.cString="<";
				     TokenList.push_back(Token);
				     break;
				case GREATERTHAN:
				     Token.Meta="GREATERTHAN";
				     Token.LineNumber=LineNumber;
				     Token.ColumnNumber=ColumnNumber;
				     Token.TokenDataType=SYMBOLGREATERTHAN;
				     Token.Data.u.cString=">";
				     TokenList.push_back(Token);
				     break;
				case ARGSEP:
				     Token.Meta="ARGSEPARATOR";
				     Token.LineNumber=LineNumber;
				     Token.ColumnNumber=ColumnNumber;
				     Token.TokenDataType=ARGSEPARATOR;
				     Token.Data.u.cString=",";
				     TokenList.push_back(Token);
				     break;
				case ENDOFLINE:
                	Token.Meta = "ENDOFLINE";
                    Token.LineNumber = LineNumber;
                    Token.ColumnNumber = ColumnNumber;
                    Token.TokenDataType = ENDOFLINETYPE;
                    Token.Data.u.cString = "\\";
                    TokenList.push_back(Token);
                   break;
				
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case '.':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':	
 	            case UNDERSCORE:
 	            case DIV:
				case PLUS:
				case MINUS:	
                    DetectAllOtherTokens(CharString, index, Token,NumberofCharacterssinFile, LineNumber,ColumnNumber);
                    break;
				
                
                default:
                	/*
                	Scan all characters.
                    */
                    break;
            }
            index++;
            ColumnNumber++;
        }
    } else
    {
        ErrorTracer::ErrorTracer(EMPTY_EXPRESSION, 0, 0);
    }

}

/*
 void Lexer::DetectAllOtherTokens(std::string &CharString, UINT32 &index, Token_tag &Token, UINT32&  NumberofCharacterssinFile, UINT32& LineNumber,UINT32& ColumnNumber)
 Arguments:
 returns: None
 Description:
 Detects numeric data such as int,float, double, short, ushort,long,ulong, long double and strings.
 Version : 0.1


    Processing Headers in File.cpp
    ---------------------
    
    #include <io.h>
    #include "Lexer.h"
        
    This function should generate  tokens for source files with headers as.
    
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER io.h
    Token - ENDHEADER
    Token - NEWLINE
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - HEADER Lexer.h
    Token - ENDHEADER
    Token - NEWLINE

 Notes:
 Function being designed.
To do:

 Detect strings for the above lines as:
  include
  io.h
  include
  lexer.h
  
  The parser would configure tokens containing strings io.h and lexer.h as Headers and any tokens such as < > or " around header names 
  as begin header, endHeader.
  
 Known issues:
 
 
 Unknown issues:
 None

 */
void Lexer::DetectAllOtherTokens(std::string &CharString, UINT32 &index, Token_tag &Token, UINT32&  NumberofCharacterssinFile, UINT32& LineNumber,UINT32& ColumnNumber)
{
    UINT32 StringType = NOTSTRING;
    //std::cout <<"In  DetectAllOtherTokens() function " << std::endl;
	// Code during definition.
    CharString += s[index];
    if ((index + 1) <=  NumberofCharacterssinFile) 
    
    {
    	
    if ( (!DS::IsHeaderCharacter(s[index+1])) )
    {
    	
            //std::cout << "Char string detected is " << CharString << std::endl;
            StringType = DS::DetectStringType(CharString);
            if (StringType == STRINGINCLUDE && (s[index + 1] == SPACE ))
            {
                //std::cout << "define string detected in DetectAllotherTokens() is " << CharString << std::endl;
                //std::cout << "index is at in DetectAllotherTokens() after detecting define " << index << std::endl;
                Token.Meta = "INCLUDE";
                Token.LineNumber = LineNumber;
                Token.ColumnNumber = ColumnNumber;
                Token.TokenDataType = INCLUDE;
                //Token.Data.u.cString = strdup(CharString.c_str());
                Token.Data.u.cString = "include";
                TokenList.push_back(Token);
                //std::cout <<"Column " << Column << std::endl;
                //Lexer::ClearToken(Token);
                CharString = "";
            }
            else if (StringType == STRINGINCLUDENEXT && (s[index + 1] == SPACE ))
            {
                //std::cout << "define string detected in DetectAllotherTokens() is " << CharString << std::endl;
                //std::cout << "index is at in DetectAllotherTokens() after detecting define " << index << std::endl;
                Token.Meta = "INCLUDENEXT";
                Token.LineNumber = LineNumber;
                Token.ColumnNumber = ColumnNumber;
                Token.TokenDataType = INCLUDENEXT;
                //Token.Data.u.cString = strdup(CharString.c_str());
                Token.Data.u.cString = "include_next";
                TokenList.push_back(Token);
                //std::cout <<"Column " << Column << std::endl;
                //Lexer::ClearToken(Token);
                CharString = "";
            }
            else if (StringType == STRINGNAME)
            {
                //std::cout << "index is at in DetectAllotherTokens()after detecting IDENT " << index << std::endl;
                Token.Meta = "NAME";
                Token.LineNumber = LineNumber;
                Token.ColumnNumber = ColumnNumber;
                Token.TokenDataType = NAME;
                //std::cout << "IDENT String detected in DetectAllotherTokens() is " << CharString << std::endl;
                Token.Data.u.cString =  (strdup(CharString.c_str()));
                //std::cout << "IDENT String detected in DetectAllotherTokens() after copying to token is " << Token.Data.u.cString << std::endl;
                TokenList.push_back(Token);
                //std::cout << "Printing token " << std::endl;
                //PrintToken(Token);
                //std::cout <<"Column " << Column << std::endl;
                //Lexer::ClearToken(Token);
                CharString = "";
            }
            
            if(DS::IsNotWordChar(s[index+1]))
            {
    	       CharString="";
	        }
    }
    }
}



#if defined (LEXER_DEVELOPMENT_DEBUGGING_ENABLED)

/*
 Component Function: void Lexer::DisplayTokenList(std::vector <Token_tag>& Tokenstream)
 Arguments:  Tokenstream vector
 returns: None
 Description:
 Displays tokens in tokenstream.
 Version : 0.1
 Notes:
 */
void Lexer::DisplayTokenList(std::vector <Token_tag>& Tokenstream)
{
    UINT32 Length = Tokenstream.size();
    UINT32 index = ZERO;
    std::string outputstring;
    //std::ostringstream ss;
    //BOOL ErrorsFound = CheckforErrors();
    BOOL ErrorsFound = false;
    if (ErrorsFound == false)
    {
        while (index < Length)
        {
            switch (Tokenstream[index].TokenDataType)
            {
                case LEFTPARENTHESIS:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token Left parenthesis " << Tokenstream[index].Data.u.cString << std::endl;;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;;
                    break;
                case RIGHTPARENTHESIS:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token Right parenthesis " << Tokenstream[index].Data.u.cString << std::endl;;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                
                case ARGSEPARATOR:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token Arg separator " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case DEFINE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token DEFINE " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case IDENT:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token Ident " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case INCLUDE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case QUOTETYPE:
				    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token QUOTE " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;   
                case MACRO:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token MACRO " << Tokenstream[index].Data.u.cString<< std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case HASHTYPE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token HASHTYPE " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber<< std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case BEGINHEADER:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token Begin Header " << Tokenstream[index].Data.u.cString<< std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
                case HEADER:    
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token Header " << Tokenstream[index].Data.u.cString<< std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;    
                case QUOTE:    
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token quote " << Tokenstream[index].Data.u.cString<< std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;     
				case ENDHEADER:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token End Header " << Tokenstream[index].Data.u.cString<< std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;	    
                case STARTOFMACROEXPRESSION:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token STARTOFMACROEXPRESSION " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
				case STARTOFMACRODECLARATION:
			      std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token STARTOFMACRODECALRATION " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
			        break;
		        case STARTOFMACRODEFINITION:
			        std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "Token STARTOFMACRODEFINITION " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
		            break; 
				#if(0)	  	    
                case WHITESPACE:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token WHITESPACE " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
				#endif	 
                case ENDOFMACRODECLARATION:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;;
                    std::cout << "End of MacroDeclaration " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;
              
                case NAME:
                    std::cout << "Token Meta " << Tokenstream[index].Meta << std::endl;
                    std::cout << "Token stringname  " << Tokenstream[index].Data.u.cString << std::endl;
                    std::cout << "Line number " << Tokenstream[index].LineNumber << std::endl;
                    std::cout << "Column number " << Tokenstream[index].ColumnNumber << std::endl;
                    break;    
                default:
                    break;
            }

            index++;
        }

    } else
    {
        ErrorTracer::PrintErrors();;
    }


}

/*
 Component Function: void Lexer::MemFreeTokenList(std::vector <Token_tag>& Tokenstream)
 Arguments:  Tokenstream vector
 returns: None
 Description:
 Clears memory allocated for tokens in tokenstream.
 Version : 0.1
 Notes:
 */
void Lexer::MemFreeTokenList(std::vector <Token_tag>& Tokenstream)
{
    UINT32 Length = Tokenstream.size();
    UINT32 index = ZERO;
    std::string outputstring;
    //std::ostringstream ss;
    BOOL ErrorsFound = ErrorTracer::CheckforErrors();
    ErrorsFound = false;
    if (ErrorsFound == false)
    {
        while (index < Length)
        {
            switch (Tokenstream[index].TokenDataType)
            {
                case HEADER:
                	/* Cast it to a non-const pointer; free takes a void*, not a const void* */
                	free(const_cast<SINT8 *>(Tokenstream[index].Data.u.cString));
                	free(const_cast<SINT8 *>(Tokenstream[index].Data.Identifier));
                	break;
                default:
                    break;
            }

            index++;
        }

    } else
    {
        ErrorTracer::PrintErrors();;
    }


}
//#endif

/*
Component Function: void Lexer::PrintToken(const Token_tag& Token)
Arguments:  Reference to token
Returns: true or not
Description:  Displays data in token.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void Lexer::PrintToken(const Token_tag& Token)
{
    // Use this code while filling Postfix and operator stack.
    switch (Token.TokenDataType)
    {
        
        case DEFINE:
            std::cout << "Token define " << Token.Data.u.cString << std::endl;
            break;
        case INCLUDE:
            std::cout << "Token include " << Token.Data.u.cString << std::endl;
            break;    
        case HASHTYPE:
            std::cout << "Token HASH " << Token.Data.u.cString << std::endl;
            break;
		case NAME:
            std::cout << "Token NAME " << Token.Data.u.cString << std::endl;
            break;	    
        case WHITESPACE:
        	std::cout << "Token Whitespace " << Token.Data.u.cString << std::endl;
            break;
        default:
            break;
    }
}


/*
Component Function: void Lexer::PrintRawToken(const Token_tag& Token)
Arguments:  Reference to token
Returns: true or not
Description:  Displays data in token.
Version : 0.1
Notes:
This function would be used while construction of AST.
 */
void Lexer::PrintRawToken(const Token_tag& Token)
{
    // Use this code while filling Postfix and operator stack.
    switch (Token.TokenDataType)
    {
  		case HASHTYPE:
		case INCLUDE:
		case HEADER:
            std::cout <<  Token.Data.u.cString;
        default:
            break;
    }
}

#endif // #if defined (LEXER_DEVELOPMENT_DEBUGGING_ENABLED)
