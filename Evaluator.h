/*
Name :  Evaluator.h
Author: Amit Malyala ,Copyright Amit Malyala 2017-Current.
Date : 14-11-16 02:08
Description:
This function uses two stacks, an intermediate header stack and a header stack and a std::vector of std::string.
and process text in them.
Notes: 
*/

/* Headers processing part of preprocesor. */
namespace Evaluator
{
    /* This function uses two stacks, an intermediate header stack and a header stack and a std::vector of std::string.*/
    void EvaluateHeaderStack(void);

}

