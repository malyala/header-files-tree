/*
Name :  Parser.h
Author: Amit Malyala , Copyright Amit Malyala 2016-Current. All rights reserved.
Date : 05-10-2016
Description:
This module provides functions for construction of AST.
*/

#ifndef PARSER_H
#define PARSER_H


#include "std_types.h"
#include "Lexer.h"
#include "stack.h"
#include "PreProcessor.h"
#include "Error.h"
#include "Modules.h"
#include <vector>

#if __cplusplus >= 201103L
/* namespace parser */
namespace Parser
{


/*
Component Function: InitParser(void)
Arguments: Tokenlist
Returns : None
Description:
Initializes parser. 
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void InitParser(void);

/*
Component Function: void ParseTokenstream(std::vector <Token_tag> &TokenStream)
Arguments: Token stream
Returns: None
Version : 0.1 Initial version.
Description:
Initializes a node and operand type. Assigns a value to a variant.
Notes:
To do:
Create another function to detect ( ) and operators, operands to prepare them to push into two stacks.
*/
void ParseTokenstream(std::vector <Token_tag> &TokenStream);

}  /* namespace Parser */

#endif /* #if __cplusplus >= 201103L */

#endif