/*
	Name:Modules.h
	Copyright:
	Author:Amit Malyala
	Date: 21-11-16 14:17
	Description:
*/

#ifndef MODULES_H
#define MODULES_H

#include "Error.h"
#include "Io.h"
#include "Io.h"
#include "Lexer.h"
#include "Stack.h"
#include "Parser.h"
#include "Datastructures.h"
#include "PreProcessor.h"

/*
Component Function:  void InitModules(void)
Arguments:  None
returns: None
Description:
Initializes all modules
Version : 0.1
*/
extern void InitModules(void);

/* Expresssion string */
extern std::vector <UINT8> s;

#endif