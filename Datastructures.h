/*
Name :  DataStructures.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
Description:
Some data structures used for Abstract sytax tree, vector tables, string to number functions,
various search and detection fuctions.
Notes:
Bug and version history:
        Version 0.1 Initial version
                0.2 Added definition of Tree node which uses a double linked list.
 */

#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <vector>
#include <string>
#include "Std_types.h"
#include <iostream>
#include "Error.h"
#include "Lexer.h"

/* Compiler switches */
/*
#define MINGW64
 */
/* Codes for Number types */

#define HEXADECIMAL_NUMBER 0x03
#define OCTAL_NUMBER 0x04
#define ALPHA_NUMERIC_NUMBER 0x05

#define NOT_OPERAND 0x02


#if __cplusplus >= 201103L
namespace DS
{

    /*
    Component Function:  void InitDataStructures(void);
    Arguments: None
    returns : None
    Version : 0.1
    Description:
    Initialize all vector tables and data structures, keyword list, symbol tables.
     */
    void InitDataStructures(void);

    /*
     Component Function: void DisplayKeywords(void)
     Arguments: None
     returns : None
     Version : 0.1
     Description:
     Display all keywords
     */
    void DisplayKeywords(void);

    /*
     Component Function: SINT32 stringtoint(const std::string & str)
     Arguments: a string which contains digits
     returns : string converted to integer
     Version : 0.1
     Description:
     Function to convert a string of Numbers into an integer.
     */

    SINT32 stringtoint(const std::string &str);



    /*
    Component Function: static std::string itostring(UINT32 a)
    Arguments:  None
    returns: None
    Description:
    Convert a decimal UINT32eger to std::string
    Version : 0.1
     */
    extern std::string itostring(const UINT32& a);
    /*
    Component Function: UINT32 IsKeyWord(std::string & Key)
    Arguments:  String to be searched in list of reserved words.
    Returns:
    Description: This function looks up a string in a vector of strings containing keywords.
    Returns keyword found or not.
    Version : 0.1
     */
    extern UINT32 IsKeyWord(std::string& key);


    /*
    Component Function: UINT32  IsHeaderCharacter(const SINT8 Character)
    Arguments:  Character to be searched in list of alphabets
    Returns:
    Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
    Returns Alphabetic or not
    Version : 0.1
    */
    UINT32  IsHeaderCharacter(const SINT8 Character);
    

    /*
     Component Function: UINT32 IsName(const SINT8* str)
     Arguments:  String characters to be searched in list of alphabets and _
     Returns:
     Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z and digits
     Returns Alphabetic or not
     Optional: Write a regex algorithm to detect alphabetical characters in a string.
     Version : 0.1
     */
    UINT32 IsName(const SINT8* str);
    
	/*
    Component Function: UINT32  IsNotWordChar(const SINT8 str)
    Arguments:  Character to be checked if its a word or not , all alphanumeric and . are part of a string.
    Returns:
    Description: This function looks up all characters such as ' ' , ''' and '"' which separate words.
    Returns if string contains the word separating characters or not.
    Version : 0.1
    */
    
     UINT32  IsNotWordChar(const SINT8 str);

    /*
    Component Function: void InitVariant(Variant &Var)
    Arguments:  None
    returns: None
    Description:
    Initializes a variant type. This function is necessary to avoid 
    a anonymous union initialization warning. Create a assign variant 
    function which can assign a value to a variant data type.
    Function to be defined. 
     * container.
    Version : 0.1
     */
    extern void InitVariant(Variant &Var);

    /*
     Component Function: UINT32 DetectStringType(const std::string& CharString)
     Arguments:  String 
     returns: None
     Description:
     Detects type of string such as define or a IDENT 
     Version : 0.1
     */
    UINT32 DetectStringType(const std::string& CharString);

    
   /*
    Component Function: UINT32  IsDigit(const SINT8 Character)
    Arguments:  Character to be searched in list of digits
    Returns:
    Description: This function looks up all characters of a string in a list of digits
    Returns Alphabetic or not
    Version : 0.1
   */
    UINT32  IsDigit(const SINT8 Character);
    
    /* Create  a string from vector of UINT8 */
    void VectortoString(std::string &newString, std::vector<UINT8> &OriginalVector);
    /* Create vector of UINT8 from string */
    void StringtoVector(std::string &OriginalString, std::vector<UINT8> &newVector);
}
#endif /* #if __cplusplus >= 201103L */

#endif /* #ifndef DATASTRUCTURES_H */