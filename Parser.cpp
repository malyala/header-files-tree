/*
Name :  Parser.cpp
Author: Amit Malyala , Copyright Amit Malyala 2016-Current.All rights reserved.
Date : 14-02-17 16:48
Version:
0.01 Initial version

Description:
A parser to detect header file names in a header or source file.

Notes:
Code being developed.

Known issues:
Unknown issues:
*/

#include "Parser.h"
/*
#include <chrono>
 */
/* For accessing Lexer token stream */
extern std::vector <Token_tag> TokenList;
/* Define a vector for Parenthesis */
std::vector <Token_tag> ParenthesisList;
/* List of initial header files in a source or header file at root level */
extern std::vector <std::string> HeaderListinOriginalSource;

/* is the parser reading original source file */
extern BOOL OriginalFileProcessed;

/*
Uncomment to test this module.
Comment to integrate with a project.
 */
#if(0)

SINT32 main(void)
{
    //std::cout << "in main() function " << std::endl;
    InitModules();
    DisplayTokenList(TokenList);

    /* std::cout << "end of main() function " << std::endl; */
    return 0;

}
#endif

/*
Component Function: void Parser::InitParser(void)
Arguments: None
Returns : None
Description:
Initializes parser.Gathers list of header names.

Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void Parser::InitParser(void)
{
    ParseTokenstream(TokenList);
}

/*
Component Function: void Parser::ParseTokenstream(std::vector <Token_tag> &TokenStream)
Arguments: Token stream from lexer output
Returns: None
Version : 0.1 Initial version.
Description:
The function would take vector of tokens as a argument and push header names into file stacks.

Notes:
To do:
Detect if a header name is on same line as #include token or same statement as a #include 
Avoid detecting strings as Token_tag in lines as void ParseTokenstream(std::vector <Token_tag> &TokenStream);

 */
void Parser::ParseTokenstream(std::vector <Token_tag> &TokenStream)
{
    /*
Measure function execution time.
auto start = std::chrono::steady_clock::now();
     */
    //std::cout << "in ParseTokenstream() function " << std::endl;

    SINT32 index = ZERO;
    const SINT32 size = TokenStream.size();
    Token_tag CurrentToken;
    Lexer::ClearToken(CurrentToken);
    Token_tag Temp;


    Lexer::ClearToken(Temp);

    SINT32 Count = ZERO;
    Token_tag NextToken;
    Token_tag PreviousToken;
    BOOL FoundNextToken = false;
    BOOL FoundPreviousToken = false;
    BOOL FoundName = false;
    BOOL FoundEndHeader = false;
    /* Check if these lines are necessary
    Stack::ClearFileStack();
    Stack::ClearOriginalSourceStack();
    */
    /* Each token in the implementation would be an element of a vector of tokens, TokenList. */
    while (index < size)
    {
        /*
        Look ahead to detect next token which is not whitespace. 
         */
        CurrentToken = TokenStream[index];

        if (CurrentToken.TokenDataType != WHITESPACE)
        {
            Count = index;
            FoundNextToken = false;
            while (Count < size && FoundNextToken == false)
            {
                if ((Count + 1) && (TokenStream[Count + 1].TokenDataType != WHITESPACE))
                {
                    /* std::cout << "Found a token which is not ' ' " << std::endl; */
                    Lexer::ClearToken(NextToken);
                    NextToken = TokenStream[Count + 1];
                    FoundNextToken = true;
                } else
                {
                    Lexer::ClearToken(NextToken);
                }
                Count++;
            }

            /* 
            Look before to detect previous token which is not whitespace.
             */
            Count = index;
            FoundPreviousToken = false;
            while (Count > 0 && FoundPreviousToken == false)
            {
                if ((Count - 1) && (TokenStream[Count - 1].TokenDataType != WHITESPACE))
                {
                    Lexer::ClearToken(PreviousToken);
                    PreviousToken = TokenStream[Count - 1];
                    FoundPreviousToken = true;
                } else
                {
                    Lexer::ClearToken(PreviousToken);
                }
                Count--;
            }

        }



        /*
         Write code to detect a header files in a source or header file such as
         #include "io.h"
         #include <lexer.h>
         
          Detect strings for the above lines as:
          include
          io.h
          include
          lexer.h

        The parser would detect tokens containing strings io.h and lexer.h as Headers and any tokens such as < > or " " around header names 
        as beginheader, endHeader.
         */
         
         /* Index is not updated after finding case include and include_next .Update index according to the variable count */
        switch (CurrentToken.TokenDataType)
        {
            case HASHTYPE:
                break;
            case DEFINE:
                break;
            case INCLUDE:
            	//system("pause");
                /* Detect header files here */
                //std::cout << "Found Include " << std::endl;
                if ((index >= 1) && (((PreviousToken.TokenDataType == HASHTYPE)) && \
					 ((NextToken.TokenDataType == SYMBOLLESSTHAN) || (NextToken.TokenDataType == QUOTETYPE))))
                {

                    /* Detect if the token following a < or " is a name " */
                    Count = index + 1;
                    FoundName = false;
                    while (Count < size && TokenStream[Count].TokenDataType != NEWLINETYPE && FoundName == false)
                    {
                        if ((Count + 1) && (TokenStream[Count + 1].TokenDataType == NAME))
                        {
                            FoundName = true;
                        }

                        Count++;
                    }

                    /* Found a header name */
                    CurrentToken = TokenStream[Count];

                    /* Check if next token to a name is > or " */
                    FoundEndHeader = false;
                    while (Count < size && TokenStream[Count].TokenDataType != NEWLINETYPE && FoundEndHeader == false)
                    {
                        if ((Count + 1) && ((TokenStream[Count + 1].TokenDataType == SYMBOLGREATERTHAN) || (TokenStream[Count + 1].TokenDataType == QUOTETYPE)))
                        {
                            FoundEndHeader = true;
                        }
                        Count++;
                    }


                    if ((FoundName) && (FoundEndHeader))
                    {
                        //std::cout << "Found a header name " << CurrentToken.Data.u.cString << std::endl;
                        if (!OriginalFileProcessed)
                        {
                            /* Push header name into file stack for intermediate stack processing */
                            
                            if (!Stack::isFileStackFull())
                            {
                            	Stack::PushFileStack(CurrentToken.Data.u.cString);
							}
							else
							{
								ErrorTracer::ErrorTracer(STACK_OVERFLOW,0,0);
							}
                        } else
                        {
                            /* Push header string name into list of header files in original source */
                            Stack::PushOriginalSourceStack(CurrentToken.Data.u.cString);
                        }

                        FoundName = false;
                        FoundEndHeader = false;
                    }
                }
                
                
                break;
            case NAME:
                break;
            case ARGSEPARATOR:
                break;
            case RIGHTPARENTHESIS:
                break;
            case LEFTPARENTHESIS:
                break;
            case NEWLINETYPE:
                break;
            case WHITESPACE:
                break;
            default:
                break;
        }
        index++;

    }
    //Measure execution time.
    //auto end = std::chrono::steady_clock::now();
    //auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    ////std::cout << "Execution time " << diff << " nanoseconds" << std::endl;
    //std::cout << "End of ParseTokenstream() function " << std::endl;
    
    TokenList.clear();
}