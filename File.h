#ifndef FILE_H
#define FILE_H

#include "Input.h"
#include "Grocery.h"

// One comment 

// Another comment 

// Three comments.

/* Four comments 
SOme more
One more
Some more
More comments*/ /* asddsfsf */ /******
******/ // This should be counted as one comment line 

// Five comments. /* C-style comment */

/* Seven and more comments here written.
*/ 

/* 
C-style comment 
*/

// Use regex on these?
/* */ // comment. This line is counted as two comments if double slash evaluation is done first. 
// comments. /* C-style comment */ This line might also be counted as two lines if c-style comment evaluation is done first. Use two tables to remove redundant lines.

/*
Another comment 
*/ // Comment here

// One more comment

/* Comment */
#endif 