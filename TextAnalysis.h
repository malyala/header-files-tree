/*
Name :  TextAnalysis.h
Author: Amit Malyala, Copyright Amit Malyala, 2017-Current. All rights reserved.
Date : 20-06-2016
Description:
This module contains a word parser function which will extract all words or strings separated
by ' '
Notes:
Bug and revision history;
0.1 Initial version
 */

#ifndef TEXT_ANALYSIS_H
#define TEXT_ANALSIS_H 

#include <vector>
#include <string>
#include "std_types.h"
#include "preprocessor.h"

namespace TextAnalysis 
{
    /*
    Component Function: void TextAnalysis(const std::vector <UINT8>& str)
    Arguments: String which is to be parsed, vector which stores all substrings in the string.
    returns : None
    Description
    Function to detect words or substrings separated by ' '.
    Version and bug history : 
    0.1 Initial version.
    */
    void TextAnalysis(const std::vector <UINT8>& str);
    /*
    Component Function: void PrintContentofHeaders(const std::vector <std::string> &Wordlist);
    Arguments: Stack of headers.
    returns : None
    Description
    Print headers and nested headers with indentation.
    Notes:
    Function being designed.
    Version and bug history : 
    0.1 Initial version.
    */
    void PrintContentofHeaders(const std::vector <std::string> &WordList);
    /*
    Component Function: void PrintHeaders(void)
    Arguments: None
    returns : None
    Description
    Print words in header files
    Notes:
    Function being designed.
    Version and bug history : 
    0.1 Initial version.
    */  
    void PrintHeaders(void);
}


#endif