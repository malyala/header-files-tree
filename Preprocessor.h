/*
Name :  PreProcessor.h
Author: Amit Malyala ,Copyright Amit Malyala 2016.
Date : 14-11-16 02:08
Description:
This project reads header files and lists names of header files. After listing header files, the program should expand header files 
and process text in them.

*/

#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H

#include "Lexer.h"
#include "CommentsProcessing.h"


// Compiler switch to select Binaryexpression tree evaluation or RPN stack evaluation at compile time.
//#define EVALUATION_METHOD_EXPRESSIONTREE 0x01
#define EVALUATION_METHOD_RPN        0x02

// Number of preprocessor passes
#define NUMBER_OF_PREPROCESSOR_PASSES 2

#define FIRST_PASS 0x01
#define SECOND_PASS 0x02
#define THIRD_PASS 0x03
#define FOURTH_PASS 0x03

#if __cplusplus >= 201103L

/* For text processing module , this is written by comments processing. This container is also used by various stacks.
Moving a whole vector and string induces some overhead with move operation?  */ 
typedef struct HeaderData
{
	std::string HeaderName; /* Header name, use c-style const char* later on for compatibility.*/
    UINT32 IndentationLevel; /* Indentation level can be 0 to 255, 300 if not processed */
	BOOL isHeaderProcessed;   /* a header has been processed to detect nested headers? true or false*/
	UINT32 NumberOfCommentLines; /* 0 if not processed */
	UINT32 NumberOfNonCommentLines; /* 0 if not processed */
	UINT32 NumberofTotalLines; /* 0 if not processed */
	std::vector <UINT8> HeaderContent; /* Content of header without comments */
} HeaderData;

/* Headers processing part of preprocessor */
namespace HeadersProcessing
{
	
/*
Component Function: void DetectHeaders(void)
Arguments: None
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void DetectHeaders(void);	
/*
Component Function: void HeadersProcessing::EvaluateHeaderStack(void)
Arguments: None
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void EvaluateHeaderStack(void);

#if(0)
/*
Component Function: void EvaluateIntermediateStack(std::vector <HeaderData> &CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
Arguments: void
Returns : None
Description:
This function evaluates all headers in intermediate stack.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void EvaluateIntermediateStack(std::vector <HeaderData> &CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed);
#endif
/*
Component Function: void HeadersProcessing::ClearHeaderData(HeaderData &CurrentHeader)
Arguments: Current header
Returns : None
Description:
Clears Header data.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void ClearHeaderData(HeaderData &CurrentHeader);


/*
Component Function: void  SetHeaderData(HeaderData& CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
Arguments: Header container and Header name name
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void SetHeaderData(HeaderData& CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed);

/*
Component Function: void LookupHeader(std::string &Filename , HeaderData &CurrentHeader)
Arguments: Look up and assign data of header into current Header.
Returns : None
Description:
This function Deletes Line numbers in CPP Comments vector which are common in C-style comments vector.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void LookupHeader(std::string &Filename , HeaderData &CurrentHeader);

}

#endif /* #if __cplusplus >= 201103L */
#endif