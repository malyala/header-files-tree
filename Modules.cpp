/*
	Name: Modules.cpp
	Copyright: Amit Malyala, 2016 All rights reserved
	Author: Amit Malyala
	Date: 21-11-16 14:17
	Description: Initializes all modules.
*/
#include "Modules.h"

/*
Component Function:  void InitModules(void)
Arguments:  None
returns: None
Description:
Initializes all modules
Version : 0.1
*/
void InitModules(void)
{
	//std::cout << "in InitModules() function " << std::endl;
    ErrorTracer::InitErrorTracer();
    //std::cout << "before InitDataStructues() function " << std::endl;
    DS::InitDataStructures();
    //std::cout << "before InitStack() function " << std::endl;
    InitStack();
	//std::cout << "end of InitModules() function " << std::endl;
}