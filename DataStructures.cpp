/*
 Name :  DataStructures.cpp
 Author: Amit Malyala , copyright 2016-Current Amit Malyala. All rights reserved.
 Date : 27-06-17 09:19
 Description:
 This module has API functions which are used by Lexer, Parser and evaluator modules.
 Notes:
  
 Build another symbol table to match parser output
 To do:
 Write output to a std::string instead of std::cout and print that string which contains output characters.


 Bug and version history:
 0.01 Initial version
 0.02 Added InitVariant() function
 0.03 Rewrote atofloat and atodouble functions
 0.04 Added #include_next feature in DetectCharString() function.

 Known issues:
 None

 Unknown issues:

 typedef signed short SINT16; gives a SINT16 does not name a type compilation error with option std=C++11.
 When the type SINT16 is used with std=c++14 - There are no errors.
*/

#include "DataStructures.h"
#include <cstring>

/*
 Component Function: static UINT32 getNumofDecimalDigits(UINT32 Number)
 Arguments:  None
 returns: Number of digits
 Description:
 Returns Number of digits in a integer
 Version : 0.1
 */
static inline UINT32 getNumofDecimalDigits(UINT32 Number);
/*
 Component static UINT32 strlength(const SINT8 *str);
 Arguments: char string
 returns : decimal digit
 Version : 0.1
 Description:
 Returns length of a char string.
 */
static UINT32 strlength(const SINT8 *str);


/*
 Component Function: static UINT8 getStrChar(UINT32 digit)
 Arguments:  None
 returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
static UINT8 getStrChar(const UINT32& digit);


// Vector table for identifiers in the program
static std::vector<Token_tag> IdentTable;


// Comment when module is integrated with the project
// Uncomment to develop or test
#if(0)

SINT32 main(void)
{
    DS::InitDataStructures();
    return ZERO;
}
#endif

/*
 Component Function: void  DS::InitDataStructures(void);
 Arguments: None
 returns : None
 Version : 0.1
 Description:
 Initialize all vector tables and data structures, keyword list, symbol tables.
 */
void DS::InitDataStructures(void)
{



}




/*
 Component static UINT32 strlength(const SINT8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
static UINT32 strlength(const SINT8 *str)
{
    UINT32 count = 0;
    while (str[count] != '\0')
    {
        count++;
    }
    return count;
}

/*
 Component Function: std::string  DS::itostring(UINT32 a)
 Arguments:  UINT32 to be converted to std::string
 returns: None
 Description:
 Convert a decimal unsigned integer to std::string
 License: GPL V3
  Version : 0.1
To do:
itostring should have line number information.
*/
std::string  DS::itostring(const UINT32& a)
{
    std::string b;
    UINT32 NumofDigits = getNumofDecimalDigits(a);

    switch (NumofDigits)
    {
        case 0:
            break;

        case 1:
            b += getStrChar(a);

            break;

        case 2:
            b += getStrChar(a / 10);
            b += getStrChar(a % 10);

            break;

        case 3:
            b += getStrChar(a / 100);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);

            break;

        case 4:
            b += getStrChar(a / 1000);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);

            break;

        case 5:
            b += getStrChar(a / 10000);
            b += getStrChar((a / 1000) % 10);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);

            break;

        case 6:
            b += getStrChar(a / 100000);
            b += getStrChar((a / 10000) % 10);
            b += getStrChar((a / 1000) % 10);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);

            break;

        case 7:
            b += getStrChar(a / 1000000);
            b += getStrChar((a / 100000) % 10);
            b += getStrChar((a / 10000) % 10);
            b += getStrChar((a / 1000) % 10);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);

            break;

        case 8:
            b += getStrChar(a / 10000000);
            b += getStrChar((a / 1000000) % 10);
            b += getStrChar((a / 100000) % 10);
            b += getStrChar((a / 10000) % 10);
            b += getStrChar((a / 1000) % 10);
            b += getStrChar((a / 100) % 10);
            b += getStrChar((a / 10) % 10);
            b += getStrChar(a % 10);
            break;

        default:
            ErrorTracer::ErrorTracer(UNABLE_TO_PROCESS_LINENUMBER, 10, 1);
            break;
    }
    return b;
}

/*
 Component Function: static UINT32 getNumofDecimalDigits(UINT32 Number)
 Arguments:  Number to detect digits
 returns: Number of digits
 Description:
 Returns number of digits in a UINT32eger
 Version : 0.1
 */

static inline UINT32 getNumofDecimalDigits(UINT32 Number)
{
    UINT32 NumofDigits = 0;
    //Number= (Number>0)? Number: -Number; // This is always true for unsigned integers
    while (Number)
    {
        Number /= 10;
        NumofDigits++;
    }
    return NumofDigits;
}

/*
 Component Function: static UINT8 getStrChar(UINT32 digit)
 Arguments:  digit as UINT32
 Returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
static UINT8 getStrChar(const UINT32& digit)
{
    UINT8 StrChar = ' ';
    switch (digit)
    {
        case 0:
            StrChar = '0';
            break;
        case 1:
            StrChar = '1';
            break;
        case 2:
            StrChar = '2';
            break;
        case 3:
            StrChar = '3';
            break;
        case 4:
            StrChar = '4';
            break;
        case 5:
            StrChar = '5';
            break;
        case 6:
            StrChar = '6';
            break;
        case 7:
            StrChar = '7';
            break;
        case 8:
            StrChar = '8';
            break;
        case 9:
            StrChar = '9';
            break;
        default:
            StrChar = '0';
            break;
    }
    return StrChar;
}


/*
 Component Function: UINT32  DS::IsHeaderCharacter(const SINT8 Character)
 Arguments:  Character to be searched in list of alphabets
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
  Version : 0.1
 */
UINT32  DS::IsHeaderCharacter(const SINT8 Character)
{
    BOOL IsHeaderChar = false;
    
    /*
      a switch statement here to detect alphabetical characters
     */
    switch (Character)
    {
        // Reading alphanumeric chars and chars in strings as filename.h
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '.':
        case '_':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '/':
		case '+':
		case '-':	
            IsHeaderChar = true;
            break;
        default:
            IsHeaderChar = false;
            break;
    }

    //std::cout << "\nin IsHeaderChar function " << std::endl;
    return IsHeaderChar;
}

/*
 Component Function: UINT32  DS::IsDigit(const SINT8 Character)
 Arguments:  Character to be searched in list of digits
 Returns:
 Description: This function looks up all characters of a string in a list of digits
 Returns Alphabetic or not
  Version : 0.1
 */
UINT32  DS::IsDigit(const SINT8 Character)
{
    BOOL IsDigit = false;
    
    /*
      a switch statement here to detect alphabetical characters
     */
    switch (Character)
    {
        // Reading alphanumeric chars and chars in strings as filename.h
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            IsDigit = true;
            break;
        default:
            IsDigit = false;
            break;
    }

    return IsDigit;
}

/*
 Component Function: UINT32  DS::IsName(const SINT8* str)
 Arguments:  String characters to be searched in list of alphabets and _
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z and a .
 Returns if string contains header characters or not.
 Optional: Write a regex algorithm to detect alphabetical characters in a string.
 Version : 0.1
 */
UINT32  DS::IsName(const SINT8* str)
{
    UINT32 i = 0;
    //UINT32 size=AlphaTable.size();
    UINT32 StrLength = strlength(str);
    UINT32 CharCount = 0;
    BOOL IsHeader = false;
    while (i <= StrLength)
    {
        /*
         a switch statement here to detect alphabetical characters
         */
        switch (str[i])
        {
                // Reading alphabetical chars
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
            case 'g':
            case 'h':
            case 'i':
            case 'j':
            case 'k':
            case 'l':
            case 'm':
            case 'n':
            case 'o':
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case 't':
            case 'u':
            case 'v':
            case 'w':
            case 'x':
            case 'y':
            case 'z':
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
            case 'G':
            case 'H':
            case 'I':
            case 'J':
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case 'O':
            case 'P':
            case 'Q':
            case 'R':
            case 'S':
            case 'T':
            case 'U':
            case 'V':
            case 'W':
            case 'X':
            case 'Y':
            case 'Z':
            case DOT:
            case PLUS:
			case UNDERSCORE:
			case MINUS:
			case DIV:	
                CharCount++;
                break;
            default:
                break;
        }
        i++;
    }

    if (CharCount >= (StrLength - 1))
    {
        IsHeader = true;
    }
    return IsHeader;
}



/*
 Component Function: UINT32  DS::IsNotWordChar(const SINT8 str)
 Arguments:  Character to be checked
 Returns:
 Description: This function looks up all characters such as ' ' , ''' and '"' which separate words.
 Returns if string contains the word separating characters or not.
 Version : 0.1
 */
UINT32  DS::IsNotWordChar(const SINT8 str)
{

    BOOL IsNotWordChar = false;
        /*
         a switch statement here to detect some characters
         */
        switch (str)
        {
            // Reading all word separating chars.
			case '\\':
			case MUL:
			case EXP:
			case SPACE:
			case NEWLINE:
			case '(':
			case ')':
			case '#':
			case ',':
			case ';':
			case  '<':
			case '>':
			case  '!':
			case '|':
			case '&':
			case '=':
			case COMPLEMENT:
			case QUOTE:
			case MODULO:
			case TILDA:
			case AT:
			case '{':
			case '}':
			case '[':
			case ']':
			    IsNotWordChar=true;
                break;
            default:
                break;
        }
    return IsNotWordChar;
}


/*
 Component Function: voidDS::InitVariant(Variant &Var)
 Arguments:  None
 returns: None
 Description:
 Initializes a variant type. This function is necessary to avoid
 a anonymous union initialization warning.
 Version : 0.1
 */
void DS::InitVariant(Variant &Var)
{
    Var.Identifier = "NONE";
    Var.Scope = "NONE";
    Var.Type = NOTADATATYPE;
    Var.u.cString = "NONE";
}


/*
 Component Function: UINT32 DS::DetectStringType(const std::string& CharString)
 Arguments:  String
 returns: None
 Description:
 Detects type of string such as define or a IDENT
 Version : 0.1
 Notes:
 Function being designed.
 Known issues:
 
 To do:
 Detect headers in #include <io.h>
 
    Processing Headers 
    ---------------------
    #include <io.h>
    #include "Lexer.h"
    
    This should display tokens with headers as include files.
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - NAME io.h
    Token - ENDHEADER
    Token - NEWLINE
    Token - HASH
    Token - INCLUDE
    Token - WHITESPACE
    Token - BEGINHEADER
    Token - NAME Lexer.h
    Token - ENDHEADER
 */
UINT32 DS::DetectStringType(const std::string& CharString)
//UINT32 DetectStringType(const SINT8* CharString, UINT32 &index, std::vector <SINT8>& s)
{
    UINT32 StringType = NOTSTRING;
    /*
    std::cout <<"In  DetectStringType() function " << std::endl;
    std::cout << "String analyzed in DetectStringType() is " << CharString << std::endl;
    */

    if (CharString == "include")
    {
        StringType = STRINGINCLUDE;
    }
    else if (CharString == "include_next")
	{
		StringType = STRINGINCLUDENEXT;
	}
	else
    {
        /* Write code to detect string which is a header such as filename.h */
        /* code edit here */
        if (DS::IsName(CharString.c_str()))
        {
            StringType = STRINGNAME;
        } 
		else
        {
            StringType = NOTSTRING;
        }
    }
    //std::cout <<"End of DetectStringType() function " << std::endl;
    
    return StringType;
}

/* Create a string from vector of UINT8 */
void DS::VectortoString(std::string &newString, std::vector<UINT8> &OriginalVector)
{
	UINT32 size = OriginalVector.size();
	UINT32 index=0;
	for(index=0;index<size;index++)
	{
		newString+=OriginalVector[index];
	}
}

/* Create vector of UINT8 from string */
void DS::StringtoVector(std::string &OriginalString, std::vector<UINT8> &newVector)
{
	UINT32 size = OriginalString.size();
	UINT32 index=0;
	for(index=0;index<size;index++)
	{
		newVector.push_back(OriginalString[index]);
	}
}
