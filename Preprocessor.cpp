/*
Name :  PreProcessor.cpp
Author: Amit Malyala ,Copyright Amit Malyala 2016-Current. All rights reserved.
Date : 08-06-17 19:11
Version: 
0.1 Initial version 
0.2 Changed SubstituteIdentifiers() , EvaluateMacroDeclarations functions, implemented getMacroEvalationString(),PrintRawToken() function
             Changed MacroParser() function
0.3 Added subexpressions in RPN format in Macro declaration which could be evaluated to produce operands for macro evaluation.    
0.4 Implemented static void EvaluateMacroSubExpressions(), SubstituteOperandsinMacroDefinitions() and SubstituteOperandsinMacroExpressions() 
    EvaluateMacroExpressions() and getMacroEvaluationString() functions 
0.5 Merged Preprocessor and Macroprocessor modules into PreProcessor.    

Description:
Write a program that reads a source file and writes out the names of files #included. Indent file names to show files #included by included files. 
Try this program on some real source files (to get an idea of the amount of information included).
Indenting means that if the program is processsing main.cpp which includes map and io.h, and map includes utility and  memory and If utility.h and 
io.h include file names bits.h and input.h. it should print using std::cout,    
    map
        utility.h
            bits.h   
        memory.h
    io.h 
        input.h
If each header includes something else,indentation should be supported upto 256 levels.

Notes:
To do:
Add support for #include_next includes 
add features for Include guards and to prevent circular inclusion of header files and prevent a header file including itself.

Implement a trie for indentation of all headers. A root node will be at indetation 0 and all nested headers will have indetations 1 or more
and are added as children of the root node.

Start with the following modules:
Io
Parts of Lexer
Parts of Parser
Parts of Preprocessor for C++ comments
Parts of DataStructures
Modules
Stack
Error tracer
TestFramework

Optional:
Write a C++ program to convert double slash to c-style comments. This program should skip double slash comments within c-style comments. There 
should not be nested c-style comments after the program is run on a C++ source or header file which contains double slash comments.

  Known Issues:
  Compiler option -std=c++98 is pointing to ISO C90 - bug in IDE
  Compiler option -std=c++03 is pointing to ISO C90 - bug in IDE
  DEV C++ 5.11 IDE is buggy, settings are sometimes corrupted. Make file configuration isn't the same as in settings. Serious Bug in IDE.

  Unknown issues:
 
 

Estimated effort:
------------------------
Software Requirements Specification    - 5 hours
Software design specificaion           - 15 hours
Software test specification            - 5 hours
Defining Software architecture         - 15 hours
Deciding on storage classes such as
static and extern functions or
Data,linkage of modules -              1 hours
Input data vector                      30 minutes
Data structures for converting
data types and arithmetic
storing Data                            1 hours
Line number and column number integration
in all modules                          5 hours
Removing unnecessary modules and
components                              5 hours
Comments Processing                     10 hours
Testcase for Comments Processing        5 hours
Testcase to check definition of
Lexer design and implementation         10 hours
Testcase to check definition            20 hours
of Lexer
Header Stack design and implementation  5 hours
Testcase for header stack               2 hours
Parser design and implementation        10 hours
Testcase to check definition            2 hours
of Parser
Header and indent detection           20 hours
Testcase to detect header and indent
detection                               10 hours.
Testcase to check definition
of Evaluator function                  2 hours
Convert all // comments to c-style
comments                               1 hours
Application program                    5 hours

Breakdown of project modules:
----------------------------------
Input data types                       1 hour
Data structures for converting         5 hours
data types and arithmetic
CommentsProcessing                    10 hours
Preprocessor core                     10 hours
Lexer design and implementation  -    20 hours
Stack design and implementation  -     5 hours
Parser design and implementation      10 hours
Application program                    5 hours

Coding log:
-----------
08-06-17 Macro processor project is taken as a baseline.Removed some modules and components. Changed file names.
         Effort estimate written down, project compiles with G++ 4.9.2 and ISO C++11 in Dev C++ 5.11
09-06-17 Created namespaces for Lexer, DS and Io, CommentsProcesssing, ErrorTracer  modules.
10-06-17 Created namespace for parser module.      
12-06-17 Corrected a bug in CommentsProcessing::BeginCommentRead which detected a single '/' token as missing comment error
17-06-17 Added compatibility switch #if __cplusplus >= 201103L ..#endif for mandatory compatibility of the project with C++11 compilers.
21-06-17 Created stack functions Push and Pop, isEmpty, isFull for HeaderStack and IntermediateHeaderStack and FileStack.
22-06-17 Added reveseFileStack() function. Added nested header levels as 256.         
23-06-17 Added void Stack::ClearFileStack(void) function to clear file stack.
         Changed HeaderInfo container to HeaderData which contains meta information of each header.
24-06-17 Added ClearHeaderData() function.Created VectortoString and StringtoVector functions in Datastructures module.
25-06-17 Implemented CommentsProcessing module to function with Lexer and parser, TextAnalysis.
26-06-17 Added Line number and column number information. Removed DetectAllOtherOperators() function in Lexer.
         Created a test case for lexer module to detect strings in #include statements in TestFramework. 
27-06-17 Removed some components in namespace DS and Lexer. Added a token NAME to detect any alphanumeric string. 
         Changed UINT32 DS::DetectStringType(const std::string& CharString) function. Modified CommentsProcessing module.
28-06-17 Added some code in Lexer module.Delete some code in DS::DetectStringType() function. Deleted some functions in DS namespace.         
         Removed some components in Parser module. Removed DetectOpType() function.
29-06-17 Created DetectHeaders function in Headersprocessing.
30-06-17 Added SetHeaderData() functoin in HeadersProcessing.
02-07-17 Created HeadersProcessing::DetectHeaders() function which detects all headers and builds a visual source tree with indentation.
04-07-17 Added some features in DetectHeaders() function.
05-07-17 Added void CommentsProcessing::AddHeaderIntoList(struct FileData &CurrentFile) function.Added LookupHeader() function.
         Created a release version.
06-07-17 Changed CommentsProcessing module, added reset features.         
28-01-19 Added #include_next feature in lexer, parser and datastructures modules.Added a few switch case statements and define constants in Lexer.h

*/

#include "std_types.h"
#include "parser.h"
#include "DataStructures.h"
#include <iostream>
#include "Modules.h"
#include "PreProcessor.h"
#include "Error.h"
#include "Stack.h"
#include <cstring>
#include <string>
#include "io.h"
#include "CommentsProcessing.h"
#include "TextAnalysis.h"
#include <chrono>

/* Get vector for storing tokens */
extern std::vector <Token_tag> TokenList;
/* Processing original file or not */
BOOL OriginalFileProcessed = false;
/*  string Containing header or source file */
extern std::vector <UINT8> s;
/* Miscellaneous data of each file processed */
extern struct FileData CurrentFile;
/* Data of each header */
extern std::vector <FileData> ListofHeaderFiles;
// UnComment this block to integrate with Preprocessor project.
//#if(0)
SINT32 main(void)
{
	UINT32 Choice=0;
	InitModules();
	do
	{
		std::cout << std::endl << "Program menu " << std::endl;
		std::cout << "1.Detect headers and display source tree of main.cpp" << std::endl;
		std::cout << "2.Print contents of headers of main.cpp" << std::endl;
		std::cout << "3.Quit";
		std::cout << std::endl << "Enter choice: ";
		std::cin >> Choice;
		switch (Choice)
		{
			case 1:
				HeadersProcessing::DetectHeaders();
				break;
			case 2:
				TextAnalysis::PrintHeaders();
			    break;
			case 3:
			default:	
			    break;		
		}
	}
	while (Choice!=3);
	
    return 0;
}
//#endif

/*
Component Function: void HeadersProcessing::DetectHeaders(void)
Arguments: None
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.01 Initial version.
0.02 Added some features in Intermediate stack processing
0.03 Added features for indentation of headers.
Notes: Function being designed. Code being developed.

Nested headers detection and processing Algorithm
-------------------------------------------------
1. List all headers into a std::vector of std::string after reading initial source or header file. All headers in this std::vector are at root 
level.The lexer and parser would  generate this list for each header or source file read.Everytime a header is read, it is first processed by 
comments processing module which detects comments and removes them. Lexer and parser are run on it.
2. Read each header in the above std::vector and run lexer and parser on it and detect if that header has nested headers .If yes, push that header 
into header stack as element of type HeaderData, gather names of nested headers in a std::vector of std::string which is called file Stack. 
Reverse this list and pop each of the header name and push into intermediateHeadersStack as element of type Headerdata. Mark indentation level of 
each header which is put into  intermediateHeaderStack. The order of pushing headers into intermediate stack should be the same as the order of
 nested headers of the above header. If the header doesn't have nested headers then push that header into headerstack.
3. Detect if the header on top of the intermediate header stack has nested headers, if yes, pop that header and push it into headerstack. 
Gather names of nested headers with lexer and parser again in a std::vector of std::string which is called File Stack. Reverse this list and 
pop each of the header name into intermediateHeadersStack as element of type Headerdata . Repeat this step until there is no header with nested 
headers on the top of intermediate headers stack.  If all nested headers in the intermediate stack dont have nested headers in them, then pop
 all of them and push them into HeaderStack. Mark indetation of each header as two, three and so on everytime it is inserted into intermediatestack. The order of pushing headers into intermediate stack should be the same as the order of headers read in root level and nested headers read at subsequent levels.
4. Repeat step 3 if intermediate headers stack is not empty.
5. If there are headers at root level which are not processed, then repeat steps 2 to 4 until all headers in the root level are pushed into 
headerstack.
6. Reverse HeaderStack to get correct order of headers and nested headers as in source or header file.



To do:

Indentation is as follows:
in main.cpp if there are headers which include other headers as

#include "map.h"
    #include "Utility.h"
        #include "bits.h"
#include "Memory.h"
        #include "input.h"    
             #include "std_types.h"
ndenting means that if the program is processsing main.cpp which includes map and input.h, and map includes utility.h and  memory.h and 
if utility.h and input.h include file names bits.h and std_types.h, it should print,    
            map
                utility.h
                     bits.h   
                memory.h
           input.h 
                std_types.h

*/
void HeadersProcessing::DetectHeaders(void)
{
	 // Measure function execution time.
    #if(0)
    auto start = std::chrono::steady_clock::now();
    #endif
    std::string Filename = "main.cpp";
    std::cout << "Input source file is: " << Filename << std::endl;
    OriginalFileProcessed = true;
    HeaderData CurrentHeader;
    std::string Headername;
    UINT32 IndentationLevelofParent=0;
    UINT32 IndentationLevelofChild=0;
    /* if header stack not empty, reset header stack */
    if (!Stack::isHeaderStackEmpty())
    {
		InitModules();
	}
    HeadersProcessing::ClearHeaderData(CurrentHeader);
    CommentsProcessing::InitCommentsProcessing(Filename);
    Lexer::InitLexer();
    Parser::InitParser();
    if (OriginalFileProcessed)
    {
        Stack::ReverseOriginalSourceStack();
        OriginalFileProcessed = false;
        //Stack::PrintOriginalSourceStack();
    }

    while (!Stack::isOriginalSourceStackEmpty())
    {
        //std::cout << "Getting a header name in original source stack "<< std::endl;
        Stack::PopOriginalSourceStack(Headername);
        /* These statements write nested header names of each header into a file stack */
        CommentsProcessing::InitCommentsProcessing(Headername);
        Lexer::InitLexer();
        Parser::InitParser();
        
        IndentationLevelofParent=0;

        /* Get header names from file stack and check if each header has nested headers */
        if (Stack::isFileStackEmpty())
        {
            /* This header has no nested headers ,push it into Header stack*/
            HeadersProcessing::ClearHeaderData(CurrentHeader);
            //std::cout << Headername << " header does not have nested headers"<< std::endl;
            HeadersProcessing::SetHeaderData(CurrentHeader, Headername, IndentationLevelofParent, true);
            //std::cout << "Pushing current header into header stack"<< std::endl;
            Stack::PushHeaderStack(CurrentHeader);
            //std::cout << "Name of header "<< Headername << std::endl;
        } else if (!Stack::isFileStackEmpty())
        {
        	/* All elements in file stack would have same indentation level.This condition would be used to set indenation of all headers detected.
        	within a header and inserted into intermediateHeaderStack.
            Elements within file stack would have indentation as IdentofChild
            */
            HeadersProcessing::ClearHeaderData(CurrentHeader);
            //std::cout << Headername << " header has nested headers"<< std::endl;
            HeadersProcessing::SetHeaderData(CurrentHeader, Headername, IndentationLevelofParent, true);
            //std::cout << "Pushing current header into header stack"<< std::endl;
            Stack::PushHeaderStack(CurrentHeader); // Changed line 
            //Stack::PushHeaderIntermediateStack(CurrentHeader);
            //system("pause");
            // Reversing file stack necessary
            Stack::ReverseFileStack();
        
            IndentationLevelofChild=IndentationLevelofParent+1;
			
            while (!Stack::isFileStackEmpty())
            {
            	/* These are header names at first level as in 
				#include "map.h"  and map.h includes "File.h" , so keep indenation of each subheader as +1.
				Each of these headers have nested headers, push them into intermediate stack*/
                Stack::PopFileStack(Headername);
                CommentsProcessing::InitCommentsProcessing(Headername);
                HeadersProcessing::ClearHeaderData(CurrentHeader);
                HeadersProcessing::SetHeaderData(CurrentHeader, Headername, IndentationLevelofChild, true);
                Stack::PushHeaderIntermediateStack(CurrentHeader);
            }
            
            /* reverse intermediate stack */
            Stack::ReverseIntermediateHeaderStack();

            /* Write features to increase indentation level of parent if there are more nested headers to process within each nested header. */
            while (!Stack::isIntermediateHeaderStackEmpty())
            {
                //std::cout << "\nInside Intermediate header stack" << std::endl;
                HeadersProcessing::ClearHeaderData(CurrentHeader);
                /* pop Current header and then push it into header stack if doesn't contain nested headers, push all nested headers
                if any into intermediate stack again.*/
                Stack::PopHeaderIntermediateStack(CurrentHeader);
                Headername = CurrentHeader.HeaderName;
                CommentsProcessing::InitCommentsProcessing(Headername);
                Lexer::InitLexer();
                Parser::InitParser();
                IndentationLevelofParent=CurrentHeader.IndentationLevel; 
         
				 
                if (!Stack::isFileStackEmpty())
                {
                	/* All elements in file stack would have same indentation level.This condition would be used to set indenation of all headers detected.
                	within a header and inserted into intermediateHeaderStack.
                	Elements within file stack would have indentation as IdentofChild
                 	A header with nested headers has been detected. Now, we will push this header into header stack and push back elements of
                 	File stack into Intermediate stack with IndentationLevel */
                 	
                    //std::cout << Headername << " header has nested headers"<< std::endl;
                    /* Push topmost header of intermediate stack into header stack after gathering its
                    nested headers .if the topmost header doesnt have nested headers, push it into header stack.
                    Change indentLevel.
                     */
                    IndentationLevelofChild=IndentationLevelofParent+1; 
    
                    Stack::PushHeaderStack(CurrentHeader);
                    /* Try setting indentation level only once for headers inserted into intermediate stack. Any nested headers of 
					a header which is put into header stack and its nested headers put in intermediate stack should not increase their indentation level.
					These nested nested headers when put onto intermediate stack are increasing their offset a lot more than their original offset should be.*/
             
                    while (!Stack::isFileStackEmpty())
                    {
                        Stack::PopFileStack(Headername);
                        CommentsProcessing::InitCommentsProcessing(Headername);
                        HeadersProcessing::ClearHeaderData(CurrentHeader);
						HeadersProcessing::SetHeaderData(CurrentHeader, Headername, IndentationLevelofChild, true);
                        //std::cout << "Pushing current header into intermediate header stack"<< std::endl;
                        Stack::PushHeaderIntermediateStack(CurrentHeader);
                    }
                } else if (Stack::isFileStackEmpty())
                {
                    CommentsProcessing::InitCommentsProcessing(Headername);
                    HeadersProcessing::ClearHeaderData(CurrentHeader);
                	/* This header has no nested headers , push it directly into header stack. It is its own parent */
                    HeadersProcessing::SetHeaderData(CurrentHeader, Headername, IndentationLevelofParent, true);
                    /* std::cout << "Pushing current header into header stack"<< std::endl; */
                    Stack::PushHeaderStack(CurrentHeader);
                }
            
            }
            
        }

    }
    /* Reverse header stack */
    Stack::ReverseHeaderStack();
    
    #if(0)
    // Measure execution time.
    auto end = std::chrono::steady_clock::now();
    //auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    //std::cout << "Execution time " << diff << " nanoseconds" << std::endl;
    std::cout << "Execution time " << diff << " milliseconds" << std::endl;
    #endif
    Stack::PrintHeaderStack();
}

/*
Component Function: void HeadersProcessing::EvaluateHeaderStack(void)
Arguments: None
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void HeadersProcessing::EvaluateHeaderStack(void)
{
    /* Function being designed */
}

#if(0)

/*
Component Function: void HeadersProcessing::EvaluateIntermediateStack(std::vector <HeaderData> &CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
Arguments: void
Returns : None
Description:
This function evaluates all headers in intermediate stack.


To do:
Automate the above to read files automatically and process all stacks created and used.

Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void HeadersProcessing::EvaluateIntermediateStack(std::vector <HeaderData> &CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
{
    /* Function being designed */

}
#endif

/*
Component Function: void HeadersProcessing::ClearHeaderData(HeaderData &CurrentHeader)
Arguments: Current header
Returns : None
Description:
Clears Header data.
Version and bug history:
0.1 Initial version.
 */
void HeadersProcessing::ClearHeaderData(HeaderData &CurrentHeader)
{
    CurrentHeader.HeaderContent.clear();
    CurrentHeader.HeaderName.clear();
    CurrentHeader.isHeaderProcessed = false;
    CurrentHeader.IndentationLevel = 300;
    CurrentHeader.NumberOfCommentLines = 0;
    CurrentHeader.NumberOfNonCommentLines = 0;
}

/*
Component Function: void  HeadersProcessing::SetHeaderData(HeaderData& CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
Arguments: Header container and Header name name
Returns : None
Description:
This function evaluates all headers.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
 */
void HeadersProcessing::SetHeaderData(HeaderData& CurrentHeader, std::string &Filename, UINT32 IndentationLevel, BOOL isHeaderProcessed)
{
	/* Gather content of each header from a vector of header names of type Current File */
    HeadersProcessing::ClearHeaderData(CurrentHeader);
    HeadersProcessing::LookupHeader(Filename ,CurrentHeader);    
    CurrentHeader.IndentationLevel = IndentationLevel;
    CurrentHeader.isHeaderProcessed = isHeaderProcessed;

}


/*
Component Function: void HeadersProcessing::LookupHeader(std::string &Filename , HeaderData &CurrentHeader)
Arguments: Look up and assign data of header into current Header.
Returns : None
Description:
Finds a header name and sets data for a header.
Version and bug history:
0.1 Initial version.
Notes: Code being developed.
*/
void HeadersProcessing::LookupHeader(std::string &Filename , HeaderData &CurrentHeader)
{
	UINT32 Size = ListofHeaderFiles.size();
	UINT32 Count=0;
	BOOL FileFound=false;
	for (Count=0;Count<Size && FileFound==false;Count++)
	{
		if (ListofHeaderFiles[Count].Filename == Filename)
		{
			CurrentHeader.HeaderName=Filename;
			CurrentHeader.HeaderContent=ListofHeaderFiles[Count].FileContent;
			CurrentHeader.NumberOfCommentLines=ListofHeaderFiles[Count].NumberofCommentLines;
			CurrentHeader.NumberOfNonCommentLines=ListofHeaderFiles[Count].NumberofNonCommentLines;
			CurrentHeader.NumberofTotalLines=ListofHeaderFiles[Count].NumberofLines;
			FileFound=true;
		}
	}
}
